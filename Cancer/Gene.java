import com.cellmatrix.EEXIST.API_V2.*;
/*
 * Definition of a single gene. The genome of the organism is comprised of a
 * matrix of genes.
 *
 * These are actual bias values (not deltas)
 *
 */
public class Gene {
  public double SX, SY, DX, DY, K; // adding karma to the gene!!!
  
  Gene copy() // make a copy
  {
	  Gene temp=new Gene();
	  temp.SX=SX;
	  temp.SY=SY;
	  temp.DX=DX;
	  temp.DY=DY;
	  temp.K=K;
	  return(temp);
  }

  void randomize()
  {
    SX=(Math.random())*40;
    SY=(Math.random())*40;
    DX=(Math.random())*40;
    DY=(Math.random())*40;
    K=(Math.random())*5;
  }
}
