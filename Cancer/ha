import com.cellmatrix.EEXIST.API_V2.*;
import java.io.PrintWriter;
import java.awt.image.BufferedImage;
import javax.imageio.ImageIO;
import java.io.File;
import java.util.Scanner;

/***
INIT:
Create 25 threads

MAIN LOOP (one generation)
 each thread, set inUse=false, done=false
 loop 50x:
   look for thread with inUse=false
   if done=true, read and save ID/score
   set inUse=true, done=false
   Load genome
   start()

(after 50x)
 Wait for all threads to be done
 sort, select, breed, mutate

 Back to MAIN LOOP
 ***/

// Thread for one individual
public class IndThread extends Thread{
  EEXIST e;
  Display disp;
  //boolean headless=true;
  boolean headless=false;
  boolean done=false;
  boolean inUse=false;
  Genome me=null;
  double finalScore=0;
  int id=0;
  public boolean phase2=false;

  //public double passedCount=0;
  boolean lastTestPassed=false; // evil globals!

  boolean DEBUG=false;

  //public boolean calculateMaxScore=false;
  //public double maxScore=0; // calculate this if above is true

// access
  public boolean isDone(){return(done);}
  public boolean isInUse(){return(inUse);}
  public void setInUse(){inUse=true;} // Core should set before calling start()...
  public void clearDone(){done=false;}
  public void setGenome(Genome g){me=g;}
  public double getScore(){return(finalScore);}
  public void setID(int id){this.id=id;}
  public int getID(){return(id);}

/************************************
 * main thread
 ************************************/

  public void run()
  {
    if (System.getenv("VT").equals("VT")){
      headless=true;
    }

    if (!inUse){ // safety dance
      System.out.println("Need to set inUse before calling start() from Core\n");
      System.exit(1);
    }

    e=new EEXIST(0,40,.125,0,40,.125);
    e.proportionalFlow(true);
    //e.debug=true;
    //e.setKarma(IndThread.readSpec("karma")); // nominally 3
//System.out.println("karma=" + IndThread.readSpec("karma"));
    int trainingSize=(int)IndThread.readSpec("tsize"); // 12
//System.out.println("trainingSize=" + trainingSize);
    if (!headless){
      disp=new Display(e);     // create a new display
      disp.setVisible(true);  // show the display
      ControlPanel cp=new ControlPanel("Control Panel");
      cp.connect(this, ControlPanel.Slider,0,2,0,400,1,"Karma","cpHandler");
    }

    finalScore=0.; // additive
    //finalScore=1.; // multiplicative
    //passedCount=0; // let's tally how many of the tests pass
    for (int cflag=1;cflag>=0;cflag--){
      for (int image=1;image<=trainingSize;image++){ // use these images for training
        me.load(e); // load my genome
        double thisScore=assess((cflag==1), image, e); // run a test, tally the score
        if (DEBUG) System.out.println("Cancerous:" + (cflag==1) + " image=" + image + " score=" + thisScore);
        System.out.println("Cancerous:" + (cflag==1) + " image=" + image + " score=" + thisScore);
        finalScore+=thisScore; // add the scores (0+40 same as 20+20)
      //thisScore=.1+(thisScore/20.); // 0.1-1.1
      //finalScore*=thisScore; // so 0/20 really hurts score!
      }
    }
    done=true; // set this first!
    inUse=false;
  }
  
/************************************
 * actual test code
 ************************************/

  double assess(boolean cancerous, int imageNum, EEXIST e) // assess this individual
  {
// filenames: raw/4[c|n]s[a|b]##.png, ## from 01 to 12
// c cancerous, n non-cancerous
// a is for training, b is for out-of-sample testing

    String filename="raw/4" + (cancerous?"c":"n") + "sa" + String.format("%02d",imageNum) + ".png";
    System.out.println("Filename=" + filename);

// load given file, step the system, check diagnosis
    double thisScore=singleTest(cancerous,filename,e);
    return(thisScore);
  }

  double singleTest(boolean cancerous, String filename, EEXIST e) // subject=1 2 or 3
  {
    double score=0; // running score
    if (true) System.out.println("Loading filename " + filename + " cancerous:" + cancerous);

    e.clearAllTubes();
// Run it! (And what we had was amazing for what it was, regardless of where it didn't lead...)

    BufferedImage img=null;
    try{
      img=ImageIO.read(new File(filename));
    } catch (Exception ex){
      System.out.println("Error reading " + filename + "(cancerous: " + cancerous + "): " + ex);
      return(0);
    }

// load chemicals
// 0->40, dx=0.125, so full address space is 320x320
// load image in top 287x287; read outputs from lower left 20x20 regions 
    for (int y=0;y<287;y+=1){
      for (int x=0;x<287;x+=1){
        int c=img.getRGB(x,y); // full pixel value
        double red=(c&0xff);
        double green=((c>>8)&0xff);
        double blue=((c>>16)&0xff);
        red=red/10.6;green=green/10.6;blue=blue/10.6; // max ~24
        double alpha=(red+blue+green)/3.; // really this is the channel that matters
        loadChems(x/16., y/16., red,green,blue,alpha); // img occupies from (0,0) to (18,18)
      }
    }

    // step 250 ticks, then read value for 20 more
    int preSteps=(int)IndThread.readSpec("presteps"); // 12
//System.out.println("presteps=" + preSteps);
    for (int i=0;i<preSteps;i++){
      e.step(1);  // pre-process/stabilize
    }

    int postSteps=(int)IndThread.readSpec("poststeps"); // 12
//System.out.println("poststeps=" + postSteps);
// sample outputs
    for (int i=0;i<postSteps;i++){ 
      e.step(1);
      double csum=readChems(10,20,14,24); // arbitrary square region
// 4x4 region, 8 ticks per step=32x32=1024 tubes. 4 elements each=4096 max val per step
    } // end of tests
    return(score); // max=# of iterations
  }

/************************************
 * chemical etc read and write
 ************************************/

// read chems in a region and tally correct ones (y shows correct value)
  double readChems(double startX,double startY,double endX,double endY)
  {
    int tally=0;
    double sum=0; // just add all chemical levels within given region
    //int maxTally=0;
    for (double x=startX;x<endX;x+=e.getDx()){
      for (double y=startY;y<endY;y+=e.getDy()){
        sum+=e.getTube(x,y,EEXIST.SRCX) + e.getTube(x,y,EEXIST.SRCY) +
             e.getTube(x,y,EEXIST.DSTX) + e.getTube(x,y,EEXIST.DSTY);
        //if ((out && sum>15) || ((!out) && sum<15)) ++tally; // correct value
        //if (calculateMaxScore) ++maxScore;
        //++maxTally;
      }
    }
    return(sum); // use this for choosing survivors etc.
  }

// load a square region of chemicals (same value throughout!)
  void loadChems(double x, double y, double v1, double v2, double v3, double v4)
  {
    e.setTube(x,y,EEXIST.SRCX,v1);
    e.setTube(x,y,EEXIST.SRCY,v2);
    e.setTube(x,y,EEXIST.DSTX,v3);
    e.setTube(x,y,EEXIST.DSTY,v4);
  }

// set a square region of diameters
  void setDiams(double startX, double startY, double endX, double endY, double value)
  {
    for (double x=startX;x<endX;x+=e.getDx()){
      for (double y=startY;y<endY;y+=e.getDy()){
        e.setDiameter(x,y,value);
      }
    }
  }

/************************************
 * legacy code...
 ************************************/

  public void cpHandler(ControlArgs arg)
  {
    switch(arg.getUserID()){
    case 1: // karma
      //e.setKarma(((double)arg.getSliderPos())/10);
      //writeStatus(pw,e); // write new EEXIST data
      System.out.println("\n\nKARMA SET TO " + e.getKarma() + "\n");
      break;
    }
  }

// configuration file processor
  static double readSpec(String type)
  {
    try{
      Scanner sc=new Scanner(new File("config_" + type));
      double ret=sc.nextDouble();
      sc.close();
      return(ret);
    } catch(Exception e){
      System.out.println("Error reading configuration file " + type + ": " + e);
      return(0);
    }
  }

}
