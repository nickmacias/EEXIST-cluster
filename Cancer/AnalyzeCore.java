import com.cellmatrix.EEXIST.API_V2.*;
//
// main execution thread. started by analyzecontrol, and used
// to run the simulation

public class AnalyzeCore extends Thread{
  int stepLimit=0;
  public boolean running;
  EEXIST e;Display disp;
  boolean checkOutput=false;
  int delayTime=0;

  public void setDelayTime(int delayTime)
  {
    this.delayTime=delayTime;
  }

  public AnalyzeCore(EEXIST e, Display disp)
  {
    this.e=e;this.disp=disp;
  }

  public void checkOutput(boolean checkOutput)
  {
    this.checkOutput=checkOutput;
  }

  public void halt() // allows halt to be requested
  {
    running=false;
    checkOutput=false; // no output monitor
  }
  
  public void myDelay(int t)
  {
    if (t==0) return; // no delay!
    try{Thread.sleep(t);} catch(Exception e){}
    return;
  }

  public void run()
  {
    int steps=0;
    running=true;
// main simulation loop
    while (running){
      e.step(1); // advance sim
      myDelay(delayTime); // pause a bit
//System.out.println("run: steps=" + steps + " limit=" + stepLimit);

      if (steps%10==0) System.out.print(".");
// finished?
      if ((stepLimit!=0) && (++steps==stepLimit)) running=false;
    }

    if (!checkOutput) return; // done

    int[] tallies=new int[20];
    for (int i=0;i<20;i++) tallies[i]=0; // running sum

// sample outputs
    for (int i=0;i<20;i++){
      e.step(1);
/*** new (separated regions) ***/

      double s1=readChems(0,15,5,20);
      double s2=readChems(10,15,15,20);
      double s3=readChems(20,15,25,20);
      double s4=readChems(5,25,10,30);
      double s5=readChems(15,25,20,30); // output regions

/*** old
      double s1=readChems(0,15,5,20);
      double s2=readChems(5,15,10,20);
      double s3=readChems(10,15,15,20);
      double s4=readChems(15,15,20,20);
      double s5=readChems(20,15,25,20); // output regions
***/

// find max, avoid typos :)
      double max=0;int maxsub=0;
      if (s1 > max){max=s1;maxsub=1;}
      if (s2 > max){max=s2;maxsub=2;}
      if (s3 > max){max=s3;maxsub=3;}
      if (s4 > max){max=s4;maxsub=4;}
      if (s5 > max){max=s5;maxsub=5;}

      ++tallies[maxsub]; // another vote for this image
      //if (subject==maxsub) ++score; // :) 

    } // end of tests
// show result
    System.out.println("\nScores: ");
    int bestScore=tallies[1];int bestIndex=1;
    for (int i=2;i<=5;i++){
      if (tallies[i] > bestScore){
        bestScore=tallies[i];bestIndex=i;
      }
    }
    System.out.print("ID: Subject " + bestIndex + " (");
    for (int i=1;i<=5;i++){
      System.out.print(tallies[i]+" ");
    }
    System.out.println(")\n----------------------------------------\n");
    running=false;
  } // thread exits here

  public void setLimit(int stepLimit)
  {
    this.stepLimit=stepLimit;
  }

// read chems in a region and tally correct ones (y shows correct value)
  double readChems(double startX,double startY,double endX,double endY)
  {
    int tally=0;
    double sum=0; // just add all chemical levels within given region
    //int maxTally=0;
    for (double x=startX;x<endX;x+=e.getDx()){
      for (double y=startY;y<endY;y+=e.getDy()){
        sum+=e.getTube(x,y,EEXIST.SRCX) + e.getTube(x,y,EEXIST.SRCY) +
             e.getTube(x,y,EEXIST.DSTX) + e.getTube(x,y,EEXIST.DSTY);
        //if ((out && sum>15) || ((!out) && sum<15)) ++tally; // correct value
        //if (calculateMaxScore) ++maxScore;
        //++maxTally;
      }
    }
    return(sum); // use this for choosing survivors etc.
  }

}
