// java Test filename

import com.cellmatrix.EEXIST.API_V2.*;
import java.awt.EventQueue;
import java.util.Scanner;
import java.io.File;
import java.awt.image.BufferedImage;
import javax.imageio.ImageIO;

class GoodCompileError{
  public static void main(String[] args)
  {
    if (args.length != 4){
      System.err.println("Usage: java Test imagefile genomefile generation individual");
      return;
    }
    Tester t=new Tester(args[0],args[1],Integer.parseInt(args[2]),Integer.parseInt(args[3])); // do the test!
  }
}

class Tester{
  double karma=3.0;
  int numPreSteps=250;
  int numTestSteps=20;
  EEXIST e;

  Tester(String filename,String genomefile,int generation, int individual)
  {
    e=new EEXIST(0,40,.125,0,40,.125);
    e.clearAllTubes();
    e.proportionalFlow(true);
    e.setKarma(karma);

    System.err.println("Processing " + filename + " genomefile=" + genomefile +
                       " gen " + generation + " Indiv " + individual);
    singleTest(filename,genomefile,generation,individual,e); // do it!
  }

// load image into EEXIST, step and analyze output
  void singleTest(String filename, String genomefile, int generation, int individual, EEXIST e)
  {
    boolean cancerous;
    if (filename.charAt(1)=='c') cancerous=true;
    else if (filename.charAt(1)=='n') cancerous=false;
    else {
      System.err.println(filename + " doesn't look like a valid filename: 2nd char should be 'c' or 'n'");
      return;
    }

    BufferedImage img=null;
    try{
      img=ImageIO.read(new File("raw/" + filename));
    } catch (Exception ex){
      System.err.println("Error reading " + filename + "(cancerous: " + cancerous + "): " + ex);
      return;
    }

// load genome
// open the file and scan
    try{
      Scanner fileScanner=new Scanner(new File(filename));
      System.err.println("Searching " + filename);
      Genome thisInd=load(fileScanner,generation,individual); // scan file and load this individual
      thisInd.load(e); // load into system
      fileScanner.close();
    }catch(Exception ex){
      System.out.println("Can't open " + filename + " : " + ex);
      return
    }

// load chemicals
// 0->40, dx=0.125, so 320x320
// load image in top 288x288; read outputs from lower left 20x20 regions
    for (int y=0;y<288;y+=1){
      for (int x=0;x<288;x+=1){
        int c=img.getRGB(x,y); // full pixel value
        double red=(c&0xff);
        double green=((c>>8)&0xff);
        double blue=((c>>16)&0xff);
        red=red/7;green=green/7;blue=blue/7; // max ~37
        double alpha=(red+blue+green)/3.; // really this is the channel that matters
        //loadChems(x/8., y/8., red, green, blue, (red+green+blue)/3.); // rgb + alpha?
        loadChems(x/16., y/16., alpha,alpha+5,alpha-7,alpha/3);
      }
    }

    // step 250 ticks, then read value for 20 more
    for (int i=0;i<numPreSteps;i++){
      if (i%10==0) System.err.println(filename + ":" + i + "/" + numPreSteps);
      e.step(1);  // pre-process/stabilize
    }

    int score=0;
// sample outputs
    for (int i=0;i<numTestSteps;i++){
      if (i%4==0) System.err.println(filename + ":" + i + "/" + numTestSteps);
      e.step(1);
      double csum=readChems(0,37.5,2.5,40);
      double ncsum=readChems(5,37.5,7.5,40);

// find max
      boolean cDiag=(csum > ncsum);
      boolean ncDiag=(ncsum > csum); // could both be false...

      if ((cancerous && cDiag) || (!cancerous && ncDiag)) ++score;
      if (i%4==0) System.err.println(filename + ":" + i + "/" + numTestSteps + " score=" +score + "/" + numTestSteps);

    } // end of tests

    System.out.println(filename + ": " + score);
  } // end of singleTest

// genomefile load code
  Genome load(Scanner sc, int tgtGen, int tgtInd)
  {
    Genome ret=new Genome();
    while (sc.hasNextLine()){
      String temp=sc.nextLine();
// comment
      if (temp.charAt(0)=='*') continue; // comment
// setup
      if (temp.charAt(0)=='S'){ // setup EEXIST
        String[] toks=temp.split(",");
        double karma=Double.parseDouble(toks[1]);
        //e.setKarma(karma); // adjust this as we scan the file
        continue;
      }
// individual
      if (temp.charAt(0)=='I'){
        String[] toks=temp.split(",");
        int gen=Integer.parseInt(toks[1]);
        int ind=Integer.parseInt(toks[2]);
        double score=Double.parseDouble(toks[3]);
        double score2=Double.parseDouble(toks[4]);
        if ((gen != tgtGen)||(ind != tgtInd)) continue;
// found the individual here
        int i=5; // index in toks[] array
        for (int x=0;x<Genome.GenomeSize;x++){
          for (int y=0;y<Genome.GenomeSize;y++){
            ret.genes[x][y].SX=Double.parseDouble(toks[i++]);
            ret.genes[x][y].SY=Double.parseDouble(toks[i++]);
            ret.genes[x][y].DX=Double.parseDouble(toks[i++]);
            ret.genes[x][y].DY=Double.parseDouble(toks[i++]);
          }
        }
        System.err.println("Score=" + score + "," + score2);
        sc.close(); // done with file for now
        return(ret); // return the genome
      }
// keep reading
    } // EOF
    sc.close();
    System.err.println("WARNING: Didn't find gen " + tgtGen +
                       ", Individual " + tgtInd);
    return(null);
  }

/************************************
 * chemical etc read and write
 ************************************/

// read chems in a region and tally correct ones (y shows correct value)
  double readChems(double startX,double startY,double endX,double endY)
  {
    int tally=0;
    double sum=0; // just add all chemical levels within given region
    //int maxTally=0;
    for (double x=startX;x<endX;x+=e.getDx()){
      for (double y=startY;y<endY;y+=e.getDy()){
        sum+=e.getTube(x,y,EEXIST.SRCX) + e.getTube(x,y,EEXIST.SRCY) +
             e.getTube(x,y,EEXIST.DSTX) + e.getTube(x,y,EEXIST.DSTY);
        //if ((out && sum>15) || ((!out) && sum<15)) ++tally; // correct value
        //if (calculateMaxScore) ++maxScore;
        //++maxTally;
      }
    }
    return(sum); // use this for choosing survivors etc.
  }

// load a square region of chemicals (same value throughout!)
  void loadChems(double x, double y, double v1, double v2, double v3, double v4)
  {
    e.setTube(x,y,EEXIST.SRCX,v1);
    e.setTube(x,y,EEXIST.SRCY,v2);
    e.setTube(x,y,EEXIST.DSTX,v3);
    e.setTube(x,y,EEXIST.DSTY,v4);
  }

}
