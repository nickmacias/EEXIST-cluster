/*
 * header file for EEXIST.cpp - NJM - Dec 2018
 */

#define DIMS 320

struct chem{
  double srcx,srcy,dstx,dsty;
};

class EEXIST
{
  private:
  chem chems[DIMS][DIMS]; // THE tubes
  chem bias[DIMS][DIMS]; // biases!
  chem memDelta[DIMS][DIMS]; // cumulative changes to each tube: tally, then apply
  int karma; // :)
  double kdelta[DIMS][DIMS]; // karma-based multiplier at given offset from (0,0)
  double dt=0.0125; // semi-random!

  public:
  EEXIST();
  void setKarma(double k);
  void load(int x, int y, chem level);
  void setBias(int x, int y, chem biases);
  chem read(int x, int y);
  void step();
  void zero_deltas();
  void doInstruction(int srcx, int srcy, int dstx, int dsty);
  void connect(int srcx, int srcy, int dstx, int dsty, double multiplier);
};
