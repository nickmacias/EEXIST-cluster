/*
 * Class definitions for EEXIST - NJM - Nov 2018
 */

#include <stdio.h>
#include <math.h>
#include "EEXIST.h"

#define abs(x) (((x)>0)?(x):(-x))

EEXIST::EEXIST() // constructor
{
  for (int x=0;x<DIMS;x++){
    for (int y=0;y<DIMS;y++){
      chems[x][y].srcx=chems[x][y].srcy=chems[x][y].dstx=chems[x][y].dsty=0;
      bias[x][y].srcx=bias[x][y].srcy=bias[x][y].dstx=bias[x][y].dsty=0;
    }
  }
// initial karma
  setKarma(30); // # of tubes
}

// set up karma-based multipliers
void EEXIST::setKarma(double k) // k=impact distance (# of tubes)
{
  karma=k; // karma is global!

// do this once and save it so we don't have to do it repeatedly!
  for (int x=0;x<DIMS;x++){
    for (int y=0;y<DIMS;y++){
      double d=sqrt(x*x+y*y); // distance from centerpoint
      if (d < k){
        kdelta[x][y]=(k-d)/k;
      } else {
        kdelta[x][y]=0.; // outside region of impact
      }
    }
  }
}

// dump chemicals into a tube
void EEXIST::load(int x, int y, chem level)
{
  chems[x][y]=level;
}

// Set bias
void EEXIST::setBias(int x, int y, chem biases)
{
  bias[x][y]=biases;
}

// read chemicals from a tube
chem EEXIST::read(int x, int y)
{
  return(chems[x][y]);
}

// step the simulation
void EEXIST::step()
{
  zero_deltas();
  for (int x=0;x<DIMS;x++){
    for (int y=0;y<DIMS;y++){
      doInstruction((int)(chems[x][y].srcx+bias[x][y].srcx),
                    (int)(chems[x][y].srcy+bias[x][y].srcy),
                    (int)(chems[x][y].dstx+bias[x][y].dstx),
                    (int)(chems[x][y].dsty+bias[x][y].dsty)); // do transfer centered here
    }
  }

// now add deltas to current chemical levels
  for (int x=0;x<DIMS;x++){
    for (int y=0;y<DIMS;y++){
      chems[x][y].srcx+=memDelta[x][y].srcx;
      chems[x][y].srcy+=memDelta[x][y].srcy;
      chems[x][y].dstx+=memDelta[x][y].dstx;
      chems[x][y].dsty+=memDelta[x][y].dsty;
    }
  }
}

// reset all transfer tallies
void EEXIST::zero_deltas()
{
  for (int x=0;x<DIMS;x++){
    for (int y=0;y<DIMS;y++){
      memDelta[x][y].srcx=memDelta[x][y].srcy=
      memDelta[x][y].dstx=memDelta[x][y].dsty=0.;
    }
  }
}

// process a single instruction
void EEXIST::doInstruction(int srcx, int srcy, int dstx, int dsty)
{
  if (karma==0){ // single transfer at centerpoint
    connect(srcx,srcy,dstx,dsty,1.0); // do one full-strength transfer
    return;
  }

// else loop across a square region
  for (int dx=-karma;dx<=karma;dx++){
    for (int dy=-karma;dy<=karma;dy++){
      double k=kdelta[abs(dx)][abs(dy)]; // multiplier
      if (k > 0) connect(srcx+dx,srcy+dy,dstx+dx,dsty+dy,k);
    }
  }
}

// do a single transfer
void EEXIST::connect(int srcx, int srcy, int dstx, int dsty, double multiplier)
{
// wrap ends
  while (srcx < 0) srcx+=DIMS; while (srcy < 0) srcy+=DIMS;
  while (dstx < 0) dstx+=DIMS; while (dsty < 0) dsty+=DIMS;
  while (srcx >= DIMS) srcx-=DIMS;while (srcy >= DIMS) srcy-=DIMS;
  while (dstx >= DIMS) dstx-=DIMS;while (dsty >= DIMS) dsty-=DIMS;

  //printf("%d %d %d %d %g\n",srcx,srcy,dstx,dsty,multiplier);

  double extra=dt*multiplier; // dx*dy*dt*karmic factor

// calculate proposed mem-deltas
// proportional flow
  double srcXSrc=memDelta[srcx][srcy].srcx - extra*chems[srcx][srcy].srcx;
  double srcYSrc=memDelta[srcx][srcy].srcy - extra*chems[srcx][srcy].srcy;
  double srcXDst=memDelta[srcx][srcy].dstx - extra*chems[srcx][srcy].dstx;
  double srcYDst=memDelta[srcx][srcy].dsty - extra*chems[srcx][srcy].dsty;
  double dstXSrc=memDelta[dstx][dsty].srcx + extra*chems[srcx][srcy].srcx;
  double dstYSrc=memDelta[dstx][dsty].srcy + extra*chems[srcx][srcy].srcy;
  double dstXDst=memDelta[dstx][dsty].dstx + extra*chems[srcx][srcy].dstx;
  double dstYDst=memDelta[dstx][dsty].dsty + extra*chems[srcx][srcy].dsty;

// and if a delta is still legal, update memDelta
  if ((chems[srcx][srcy].srcx+srcXSrc >= 0) && (chems[dstx][dsty].srcx+dstXSrc < DIMS)){
    memDelta[srcx][srcy].srcx=srcXSrc;memDelta[dstx][dsty].srcx=dstXSrc;
  }
  if ((chems[srcx][srcy].srcy+srcYSrc >= 0) && (chems[dstx][dsty].srcy+dstYSrc < DIMS)){
    memDelta[srcx][srcy].srcy=srcYSrc;memDelta[dstx][dsty].srcy=dstYSrc;
  }
  if ((chems[srcx][srcy].dstx+srcXDst >= 0) && (chems[dstx][dsty].dstx+dstXDst < DIMS)){
    memDelta[srcx][srcy].dstx=srcXDst;memDelta[dstx][dsty].dstx=dstXDst;
  }
  if ((chems[srcx][srcy].dsty+srcYDst >= 0) && (chems[dstx][dsty].dsty+dstYDst < DIMS)){
    memDelta[srcx][srcy].dsty=srcYDst;memDelta[dstx][dsty].dsty=dstYDst;
  }
// all done :)
}
