import com.cellmatrix.EEXIST.API_V2.*;
/*
 * Definition of a single gene. The genome of the organism is comprised of a
 * matrix of genes.
 *
 * These are actual bias values (not deltas)
 *
 */
public class Gene {
  public double SX, SY, DX, DY;
  
  Gene copy() // make a copy
  {
	  Gene temp=new Gene();
	  temp.SX=SX;
	  temp.SY=SY;
	  temp.DX=DX;
	  temp.DY=DY;
	  return(temp);
  }

  void randomize()
  {
    SX=(Math.random())*40;
    SY=(Math.random())*40;
    DX=(Math.random())*40;
    DY=(Math.random())*40;
  }
}
