import com.cellmatrix.EEXIST.API_V2.*;
import java.io.PrintWriter; 

// image processing
import java.awt.image.BufferedImage;
import javax.imageio.ImageIO;
import java.io.File;

public class Core extends Thread{
  
  public void run()
  {
    EEXIST e=new EEXIST(0,320,1,0,320,1);
    e.proportionalFlow(true);
    int next=12;
    for (int x=0;x<320;x++){
      for (int y=0;y<320;y++){
        e.setTube(x,y,0,(next++)%320);
        e.setTube(x,y,1,(next++)%320);
        e.setTube(x,y,2,(next++)%320);
        e.setTube(x,y,3,(next++)%320);
      }
    }
    e.setKarma(30);
    System.out.println("Ready to step...");
    e.step(1);
    System.out.println("Done!");
  }
}
