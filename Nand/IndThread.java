import com.cellmatrix.EEXIST.API_V2.*;
import java.io.PrintWriter;

/***
INIT:
Create 25 threads

MAIN LOOP (one generation)
 each thread, set inUse=false, done=false
 loop 50x:
   look for thread with inUse=false
   if done=true, read and save ID/score
   set inUse=true, done=false
   Load genome
   start()

(after 50x)
 Wait for all threads to be done
 sort, select, breed, mutate

 Back to MAIN LOOP
 ***/

// Thread for one individual
public class IndThread extends Thread{
  EEXIST e;
  Display disp;
  boolean headless=true;
  boolean done=false;
  boolean inUse=false;
  Genome me=null;
  double finalScore=0;
  int id=0;
  public boolean phase2=false;

  public double passedCount=0;
  boolean lastTestPassed=false; // evil globals!

  boolean DEBUG=false;

  public boolean calculateMaxScore=false;
  public double maxScore=0; // calculate this if above is true

// access
  public boolean isDone(){return(done);}
  public boolean isInUse(){return(inUse);}
  public void setInUse(){inUse=true;} // Core should set before calling start()...
  public void clearDone(){done=false;}
  public void setGenome(Genome g){me=g;}
  public double getScore(){return(finalScore);}
  public void setID(int id){this.id=id;}
  public int getID(){return(id);}

/************************************
 * main thread
 ************************************/

  public void run()
  {
    if (!inUse){ // safety dance
      System.out.println("Need to set inUse before calling start() from Core\n");
      System.exit(1);
    }

    e=new EEXIST(0,40,.25,0,40,.25);
    e.proportionalFlow(true);
    //e.debug=true;
    e.setKarma(2);
    if (!headless){
      disp=new Display(e);     // create a new display
      disp.setVisible(true);  // show the display
      ControlPanel cp=new ControlPanel("Control Panel");
      cp.connect(this, ControlPanel.Slider,0,5,0,400,1,"Karma","cpHandler");
    }

    finalScore=0.;
    passedCount=0; // let's tally how many of the (160) tests pass
    for (int test=0;test<8;test++){
      me.load(e); // load my genome
      double thisScore=assess(test,e); // run a test, tally the score
      if (DEBUG) System.out.println("Indiv " + id + " test=" + test + " score=" + thisScore);
System.out.println("Indiv " + id + " test=" + test + " score=" + thisScore);
      if (!phase2){finalScore+=thisScore;} else
                  {finalScore*=thisScore;}
    }
    done=true; // set this first!
    inUse=false;
  }
  
/************************************
 * actual test code
 ************************************/

  double assess(int testNum,EEXIST e) // assess this individual
  {
    boolean a=((testNum&1) != 0);
    boolean b=((testNum&2) != 0);
    boolean c=((testNum&4) != 0);
    maxScore=0;calculateMaxScore=true;
    double thisScore=singleTest(a,b,c,testNum,e);
    if (thisScore==0) thisScore=.5; // so we don't zero out other info
    return(thisScore/maxScore);
  }

  double singleTest(boolean a, boolean b, boolean c, int testNum,EEXIST e)
  {

// %%% Test Function Here

    //boolean out=!(a&b&c); // nand
    //boolean out=!(a|b|c); // nor
    boolean out=(a^b^c); // XOR
    
    double score=0; // running score

    e.clearAllTubes();

    // input a is placed in [0,4]; b in [8,12]; c in [16,20]; out is read from [24,28]
    //loadChems(0,0,4,4,a?20:5); setDiams(0,0,4,4,1);
    //loadChems(8,8,12,12,b?20:5); setDiams(8,8,12,12,1);
    //loadChems(16,16,20,20,c?20:5); setDiams(16,16,20,20,1); // or set diam=0 :)
    loadChems(0,0,8,8,a);
    loadChems(8,8,16,16,b);
    loadChems(16,16,24,24,c);

    //loadChems(24,24,32,32,10,15,20,25); // dump some chemicals in to work with

    // step 250 ticks, then read value for 20 more
    for (int i=0;i<250;i++){
      if (DEBUG) System.out.println("Indiv " + id + " test=" + testNum + " pre-step " + i);
      e.step(1);  // pre-process/stabilize
    }
    for (int i=0;i<50;i++){ 
      if (DEBUG) System.out.println("Indiv " + id + " test=" + testNum + " step " + i);
      e.step(1);
      //score+=readChems(out,27,27,29,29); // read chem levels in region, and adjust score accordingly
      score+=readChems(out,29,29,30,30); // read chem levels in region, and adjust score accordingly
      if (lastTestPassed) ++passedCount;
    } // end of tests
    return(score);
  }

/************************************
 * chemical etc read and write
 ************************************/

// read chems in a region and tally correct ones (y shows correct value)
  int readChems(boolean out, double startX,double startY,double endX,double endY)
  {
    int tally=0;
    int maxTally=0;
    for (double x=startX;x<endX;x+=e.getDx()){
      for (double y=startY;y<endY;y+=e.getDy()){
        double sum=e.getTube(x,y,EEXIST.SRCX) + e.getTube(x,y,EEXIST.SRCY) +
                   e.getTube(x,y,EEXIST.DSTX) + e.getTube(x,y,EEXIST.DSTY);
        if ((out && sum>15) || ((!out) && sum<15)) ++tally; // correct value
        if (calculateMaxScore) ++maxScore;
        ++maxTally;
      }
    }
// we're done with this region. See if at least 75% of the tubes
// registered as "1"
    lastTestPassed=(((double)tally)/((double)maxTally) >= .75);
    return(tally); // use this for choosing survivors etc.
  }

// load a square region of chemicals (same value throughout!)
  void loadChems(double startX, double startY, double endX, double endY, double v1, double v2, double v3, double v4)
  {
    for (double x=startX;x<endX;x+=e.getDx()){
      for (double y=startY;y<endY;y+=e.getDy()){
        e.setTube(x,y,EEXIST.SRCX,v1);
        e.setTube(x,y,EEXIST.SRCY,v2);
        e.setTube(x,y,EEXIST.DSTX,v3);
        e.setTube(x,y,EEXIST.DSTY,v4);
      }
    }
  }

// load a square region of chemicals based on a boolean input
  void loadChems(double startX, double startY, double endX, double endY, boolean value)
  {
    double xAmt,yAmt;
    if (value){xAmt=25;yAmt=10;} else {xAmt=15; yAmt=20;}
    for (double x=startX;x<endX;x+=e.getDx()){
      for (double y=startY;y<endY;y+=e.getDy()){
        e.setTube(x,y,EEXIST.SRCX,xAmt);
        e.setTube(x,y,EEXIST.SRCY,yAmt);
        e.setTube(x,y,EEXIST.DSTX,xAmt);
        e.setTube(x,y,EEXIST.DSTY,yAmt);
      }
    }
  }

// set a square region of diameters
  void setDiams(double startX, double startY, double endX, double endY, double value)
  {
    for (double x=startX;x<endX;x+=e.getDx()){
      for (double y=startY;y<endY;y+=e.getDy()){
        e.setDiameter(x,y,value);
      }
    }
  }

/************************************
 * legacy code...
 ************************************/

  public void cpHandler(ControlArgs arg)
  {
    switch(arg.getUserID()){
    case 1: // karma
      e.setKarma(((double)arg.getSliderPos())/10);
      //writeStatus(pw,e); // write new EEXIST data
      System.out.println("\n\nKARMA SET TO " + e.getKarma() + "\n");
      break;
    }
  }

}
