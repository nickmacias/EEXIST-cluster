import com.cellmatrix.EEXIST.API_V2.*;
import java.io.PrintWriter; 

public class Core extends Thread{
  
  int numCores=25; // YES!

  int popSize=25; // total # of individuals in the population
  int survivorSize=5; // # of individuals to keep after each generation

  PrintWriter pw;
  boolean DEBUG=false;
  double maxScore=0;

// the population!
  Genome[] population=new Genome[popSize];
  Genome[] best=new Genome[survivorSize]; // save best members here
  double[] score=new double[popSize]; // save scores
  IndThread[] cores=new IndThread[numCores];

// here's where we run the entire simulation...
  public void run()
  {
    try{
      pw=new PrintWriter("raw.txt");
    } catch(Exception e){
      System.out.println("Can't write to output file ("+e+") - goodbye");
      return;
    }

// %%% put these in vars, and pass to IndThread()

    writeStatus(pw,"0,40,.25,0,40,.25,4.0"); // write relevant info about EEXIST
    int gen=0; // track generations

// make an initial population
    for (int i=0;i<popSize;i++){ 
      population[i]=new Genome();
      population[i].randomize();
    }

// create threads
    for (int i=0;i<numCores;i++){
      cores[i]=new IndThread();
    }

    boolean[] processed=new boolean[popSize]; // record which individuals have finished

    double numPassedTests;
// main simulation loop
    while (true){
      ++gen;
      System.out.println("Beginning generation " + gen);

      for (int indiv=0;indiv<popSize;indiv++){
        processed[indiv]=false; // need to clear these before loop (async)
      }

      for (int i=0;i<numCores;i++){
        cores[i]=new IndThread();
      }

      for (int indiv=0;indiv<popSize;indiv++){

// - find a free thread
// - save its results if it ran an assessment
// - load genome and start

        int freeThread=findFreeThread(); // check, sleep, eventually return
        if (DEBUG) System.out.println("gen " + gen + "indiv " + indiv + " Found free thread #" + freeThread);

        if (cores[freeThread].isDone()){ // this thread finished an assessment
          int who=cores[freeThread].getID(); // individual # of thread that finished
          score[who]=cores[freeThread].getScore(); // save that individual's score
          numPassedTests=cores[freeThread].passedCount;
          if ((gen==1)&&(who==0)){
            maxScore=cores[freeThread].maxScore;
            pw.println("*M,"+maxScore);
          }
          writeIndiv(pw,gen,who,numPassedTests,population[who],score[who]); // save to logfile
          System.out.print(gen + ":" + who + "(" + score[who] + "/" + maxScore + ")\n");
          processed[who]=true; // this individual is done
        }

// now process individual #indiv on this thread
        cores[freeThread]=new IndThread(); // if we try to start() the same thread, we get a java.lang.IllegalThreadStateException
        if ((gen==1) && (indiv==0)) cores[freeThread].calculateMaxScore=true;
        ///if (gen > 12) cores[freeThread].phase2=true; // next phase!
	cores[freeThread].setInUse();
        cores[freeThread].clearDone(); // prepare for next run
        if (DEBUG) System.out.println("Clearing done on thread " + freeThread);
        cores[freeThread].setGenome(population[indiv]); // load this individual's genome
        cores[freeThread].setID(indiv); // save for debug
        if (DEBUG) System.out.println("Starting gen " + gen + " indiv " + indiv + " on thread " + freeThread);
        cores[freeThread].start(); // run it!
      } // end of evaluation of all individuals

// all individuals are started (and some are probably finished)
// wait for all individuals to be processed

      boolean doMore=true;
      while (doMore){
        for (int core=0;core<numCores;core++){
          if (cores[core].isDone()){ // this thread finished an assessment
            cores[core].clearDone(); // done with this core
            int who=cores[core].getID(); // individual # of thread that finished
            if (!processed[who]){ // haven't recorded this individual yet
              score[who]=cores[core].getScore(); // save that individual's score
              numPassedTests=cores[core].passedCount;
              writeIndiv(pw,gen,who,numPassedTests,population[who],score[who]); // save to logfile
              System.out.print(gen + " : " + who + "(" + score[who] + ")\n");
              processed[who]=true; // this individual is done
            }
          }
        } // all cores managed (either idle or still running an individual)

// see if everyone has been processed
        doMore=false; // set if we find a thread that's still running
        for (int i=0;i<popSize;i++){
          if (!processed[i]) doMore=true;
        }
        if (doMore) sleep(500); // take a break
      } // loop until all cores show Done

      System.out.println("\nGen " + gen + " Best score=" + rank() + "\n"); // rank all individuals
      breed(.10); // breed them, with mutation rate of 5%
    } // and repeat forever
  }

  int findFreeThread() // find next available thread
  {
    while (true){
      for (int thisCore=0;thisCore<numCores;thisCore++){
        if (!cores[thisCore].isInUse()) return(thisCore); // found one!
      }
      sleep(500); // wait 500 mS
    }
  }

// wait a bit
  void sleep(int t)
  {
    try{Thread.sleep(t);} catch(Exception e){}
  }
  
  double rank() // sort population[] by score[]
  {
    for (int loop=0;loop<popSize;loop++){ // good old bubble sort!
      for (int i=0;i<popSize-1;i++){ // compare score[i] with score[i+1]
        if (score[i+1] > score[i]){ // swap
          double temp=score[i+1];score[i+1]=score[i];score[i]=temp;
          Genome temp2=population[i+1];population[i+1]=population[i];population[i]=temp2;
        }
      }
    }

// copy best elements of population to best[] array
    double bestScore=0;
    for (int i=0;i<survivorSize;i++){
      best[i]=population[i].copy();
      bestScore+=score[i];
    }
    return(score[0]);
  }
  
  void breed(double mutateRate) // copy best individuals to best[] array, then breed them into the population[] array
  {
    Genome[] savePop=new Genome[popSize];

// save a copy of current population
    for (int i=0;i<popSize;i++){
      savePop[i]=population[i].copy();
    }

// now merge members
    for (int i=0;i<popSize;i++){
      if (i<survivorSize){
        population[i]=best[i].copy();
      } else { // merge 2 members
        population[i]=best[(int)(Math.random()*survivorSize)].copy();
        population[i].merge(savePop[(int)(Math.random()*popSize)]);
        population[i].mutate(mutateRate); // random variation
      }
    }
  }

  boolean firstWS=true;
  void writeStatus(PrintWriter pw,String s)
  {
    if (firstWS){
      pw.println("*S,"+s);
      firstWS=false;
    }
    pw.flush();
  }

  boolean firstWI=true;

  void writeIndiv(PrintWriter pw,int gen,int indiv,double numPass,Genome genome,double score)
  {
    if (firstWI){
      pw.println("*I,gen,ind,score,numPass,initSX[x][y],initSY[x][y],initDX[x][y],initDY[x][y],delta[x][y],deltaSX[x][y],deltaSY[x][y],deltaDX[x][y],deltaDY[x][y]");
      firstWI=false;
    }
    pw.print("I,"+gen+","+indiv+","+score+","+numPass);
    for (int x=0;x<Genome.GenomeSize;x++){
      for (int y=0;y<Genome.GenomeSize;y++){
        pw.print(","+
               genome.genes[x][y].initSX+"," +
               genome.genes[x][y].initSY+"," +
               genome.genes[x][y].initDX+"," +
               genome.genes[x][y].initDY+"," +
               genome.genes[x][y].delta+","  +
               genome.genes[x][y].deltaSX+","+
               genome.genes[x][y].deltaSY+","+
               genome.genes[x][y].deltaDX+","+
               genome.genes[x][y].deltaDY);
      }
    }
    pw.println();pw.flush();
  }
}
