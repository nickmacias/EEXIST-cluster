class FFT{
  static int NUMFREQ=300;
  static double OMEGA0=.1;

  static double[] doFFT(double[] rawData) // convert from time to freq domain
  {
    double value,dt,t;

    double a,b;
    double[] bcoef=new double[NUMFREQ];
    double[] acoef=new double[NUMFREQ];
    double[] combo=new double[NUMFREQ];

    double length = rawData.length;
    
      for (double n = 0; n < NUMFREQ; n++){
      double freq=OMEGA0*n;
        a=b=0;
        dt=1./44100.;t=0;
        for(int index=0;index<rawData.length;index++){
          value=rawData[index]; // next sample
          a+=value*Math.cos(freq*t)*dt;
          b+=value*Math.sin(freq*t)*dt;
          t=t+dt;
        }
        
        a=a*2./t;b=b*2./t;
        if (n==0){a=a/2;b=0;}
          acoef[(int)n]=a;
          bcoef[(int)n]=b;
          combo[(int)n]=Math.sqrt(a*a+b*b);
      }
      return(combo);
  }
}
