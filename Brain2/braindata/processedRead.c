#include <stdio.h>

void process(int* data, int len); // packet processor
void dump(); // display data
void qout(int); // print "X" if quality is poor

// grab processed data (freq domain?)

int main()
{
// State Machine
//  1: need AA
//  2: need second AA
//  3: need packetLength
//  4: reading packetLength
//  5: waiting for checksum

  int packetLength;
  int packetSum;
  int packetData[256];
  int packetIndex;

  int c;
  int state=1;
  while (EOF != (c=getchar())){
    switch(state){
      case 1: // need first sync pulse
              if (c==0xAA) state=2;
              break;
      case 2: // need second sync pulse
              if (c == 0xAA){ // yes!
                state=3;
              } else state=1; // was bogus
              break;
      case 3: // read packet length
              if (c==0xAA){ // sync pulse
                state=3; // (nop...length comes next)
              } else if (c > 170){ // error
                state=1;
              } else {
                packetLength=c;
                packetIndex=0; // for storing packet in array
                packetSum=0; // for calculating checksum
                state=4;
              }
              break;
      case 4: // reading packet
              packetData[packetIndex++]=c; // save data
              packetSum+=c; // calculate checksum
              if (packetIndex==packetLength) state=5; // ready for checksum
              break;
      case 5: // just got checksum byte
              if ((packetSum&0xff) + c != 255){
                fprintf(stderr,"Checksum error: %02x vs %02x\n",
                  packetSum&0xff,c);
                state=1;
              } else {
                process(packetData,packetLength);
                state=1;
              }
    }
  }
  fprintf(stderr,"EOF\n");
}

int quality=255; // want 0

// parse packet, pick out raw data etc.
void process(int *packet, int packetLength)
{
  //dump(packet,packetLength); // echo
  int ind=0; // move through packet
  int data;
  while (ind < packetLength){
    switch (packet[ind++]){
      case 0x02: // signal quality
        quality=packet[ind++];
        printf("Q %d\n",quality);
        break;
      case 0x80: // raw data
        data=packet[ind+1]<<8 | packet[ind+2]; // raw value
        if (data > 32767) data=data-65536;
        qout(quality); // print preamble if quality is low
        printf("R %d\n",data);
        ind+=3;break; // (we ignored the length argument)
      case 0x04: // attention
        qout(quality);
        printf("A %d\n",packet[ind++]);break;
      case 0x05: // meditation
        qout(quality);
        printf("M %d\n",packet[ind++]);break;
      case 0x83: // ASIC_EEG_POWER
        if (packet[ind] != 24){ // error?
          fprintf(stderr,"ERROR: ASIC_EEG_POWER packet length=%d, expecting 24\n",packet[ind]);return;
        }
        ++ind;
        qout(quality);
        printf("WDEL %d\n",packet[ind]<<16|packet[ind+1]<<8|packet[ind+2]);
        ind+=3;
        qout(quality);
        printf("WTHE %d\n",packet[ind]<<16|packet[ind+1]<<8|packet[ind+2]);
        ind+=3;
        qout(quality);
        printf("WLAL %d\n",packet[ind]<<16|packet[ind+1]<<8|packet[ind+2]);
        ind+=3;
        qout(quality);
        printf("WHAL %d\n",packet[ind]<<16|packet[ind+1]<<8|packet[ind+2]);
        ind+=3;
        qout(quality);
        printf("WLBE %d\n",packet[ind]<<16|packet[ind+1]<<8|packet[ind+2]);
        ind+=3;
        qout(quality);
        printf("WHBE %d\n",packet[ind]<<16|packet[ind+1]<<8|packet[ind+2]);
        ind+=3;
        qout(quality);
        printf("WLGA %d\n",packet[ind]<<16|packet[ind+1]<<8|packet[ind+2]);
        ind+=3;
        qout(quality);
        printf("WMGA %d\n",packet[ind]<<16|packet[ind+1]<<8|packet[ind+2]);
        ind+=3;
        break;
      default: fprintf(stderr,"Unknown code: %02x: giving up on packet\n",
                       packet[ind-1]);return;
    }
  }
}

void qout(int quality) // print preamble if quality is low
{
  if (quality != 0) printf("X");
}

// display packet data
void dump(int *packet, int packetLength)
{
  for (int i=0;i<packetLength;i++){
    printf("%02x ", packet[i]);
  }
  printf("\n");
}
