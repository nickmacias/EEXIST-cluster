//
// control panel class
//


import com.cellmatrix.EEXIST.API_V2.*;
import java.awt.EventQueue;
import java.util.Scanner;
import java.io.File;
import java.awt.image.BufferedImage;
import javax.imageio.ImageIO;

public class AnalyzeControl extends Thread{
  EEXIST e;
  Display disp;
  AnalyzeCore core=null; // create this when we want to run forward

  int NUMBEROFSTEPS=250;
  double KARMA=2.0;

  String fileName;
  Scanner fileScanner=null; // for reading from raw file

  int delayTime=0; // pass this to core before running a test

  Genome thisInd=null; // loaded from file

  int DEL=0;
  int THE=1;
  int LAL=2;
  int HAL=3;
  int LBE=4;
  int HBE=5;
  int LGA=6;
  int MGA=7;

  String bandNames[]={
                      "Delta",
                      "Theta",
                      "Low Alpha",
                      "High Alpha",
                      "Low Beta",
                      "High beta",
                      "Low Gamma",
                      "Medium Gamma"};

// main method: construct EEXIST, display and control panel
// then idle until user starts thread

  public void run()
  {
    e=new EEXIST(0,40,.125,0,40,.125); // main EEXIST object
    e.proportionalFlow(true);
    e.setKarma(KARMA);

    disp=new Display(e); // create a new display
    disp.setVisible(true);  // show the display

    ControlPanel cp=new ControlPanel("Control Panel");
    //cp.connect(this,ControlPanel.Slider,0,10,0,400,1,"Karma","cpHandler");
    //cp.connect(this,ControlPanel.CheckBox,0,10,"Run/Pause","cpHandler");
    cp.connect(this,ControlPanel.TextIn,0,11,"Command","cpHandler");
  }
// gene.load(e);e.step(1);
  
  public void cpHandler(ControlArgs arg)
  {
    switch(arg.getUserID()){
    case 1: // karma
      //e.setKarma(((double)arg.getSliderPos())/10);
      System.out.println("\n\nKARMA SET TO " + e.getKarma() + "\n");
      break;
    case 10: // Run/Pause
      //System.out.println(arg.getCheckState()?"Running":"Paused");
      break;
    case 11: // text input
      System.out.println("<"+arg.getTextIn()+">");
      if (!parse(arg.getTextIn())) System.exit(0); // returns FALSE after Q command
      break;
    }
  }

  boolean parse(String s)
//
// r - run forever
// r n - run n steps
// h - halt
// f filename - name input file
// l gen ind - load this individual
// l - reload last individual
// t b0 b1 - run a test!
// x - load a random tube (debug gadget)
// Q - quit all
//
  {
    String[] sa=s.split(" ");
    for (int i=0;i<sa.length;i++){
      System.out.println("["+i+"]:<"+sa[i]+">");
    }
    if (sa.length < 1) return(true); // just an ENTER?

// QUIT
    if (sa[0].equals("Q")){
      if (core!=null) core.halt();
      return(false);
    }

// RUN commands
    if (sa[0].equals("h")){
      if (core != null){
        core.halt(); // signal core to exit
        core=null; // and remember that it's gone
        return(true);
      }
    }
    if (sa[0].equals("r")){
      if (core==null){
        core=new AnalyzeCore(e,disp);
      }
// is a length specified?
      if (sa.length==2){ // yes
        try{
          core.setLimit(Integer.parseInt(sa[1]));
        }catch(Exception e){
          System.out.println("Error parsing " + sa[1]);return(true);
        }
        core.start(); // start the thread
        core=null; // since this thread will exit shortly
      } else {
        core.setLimit(0); // no limit
        core.start(); // start the core thread; will exit after HALT of when done
      }
    }

// set delay
    if (sa[0].equals("d")){
      if (sa.length==2){ // yes
        delayTime=Integer.parseInt(sa[1]);
        System.out.println("Delay set to " + delayTime);
      }
      return(true);
    }

// test everything (Destroy All Monsters!)
    if (sa[0].equals("a")){
      int[] maxSamples={11,6,6}; // 118 Math records; 61 Mozart; 64 Spongebob

// make sure we have an AnalyzeCore object
      if (core==null){
        core=new AnalyzeCore(e,disp);
      }

// sweep all tests
      for (int test=2;test>=0;test--){
        for (int sample=0;sample<maxSamples[test];sample++){
          System.out.println("\n%Test " + test + " sample " + sample +"%");
          singleTest(test,sample,e); // load data
          core.running=true;
          core.setLimit(NUMBEROFSTEPS);
          core.setDelayTime(delayTime);
          core.checkOutput(true); // tally and output score at end
          core.run(); // don't start(): run in current thread
        }
      }
      return(true);
    } // end of all tests!

// test :)
    if (sa[0].equals("t")){ // t test# sample#
      if (sa.length == 3){ // okay :)
// get subject and pose
        int test=Integer.parseInt(sa[1]); // 0-2
        int sample=Integer.parseInt(sa[2]); // train=0
        singleTest(test,sample,e); // load these inputs
// now tell the core system to tally the output, and run 50 steps
        if (core==null){
          core=new AnalyzeCore(e,disp);
        }
        core.running=true;
        core.setLimit(NUMBEROFSTEPS);
        core.setDelayTime(delayTime);
        core.checkOutput(true); // tally and output score at end
        core.start();
/***
        while (core.running){
          try{
            Thread.sleep(100);
          } catch(Exception e){
          }
        }
***/
        core=null;
        return(true);
      } // end of test

    } // end of "t"


// file
    if (sa[0].equals("f")){
      if (sa.length<2) return(true); // need a filename
      fileName=sa[1]; // save this
      try{
        fileScanner=new Scanner(new File(fileName));
        System.out.println(fileName + " opened succesfully for reading");
        fileScanner.close();
      }catch(Exception e){
        System.out.println("ERROR: Can't open " + sa[1]);
      }
      return(true);
    }

// random
    if (sa[0].equals("x")){
      e.setTube(40*Math.random(),40*Math.random(),(int)(2.*Math.random()),40.*Math.random());
      return(true);
    }

// load an individual
    if (sa[0].equals("l")){ // check usage
      if (sa.length==1){ // load last individual
        e.clearAllTubes();
        thisInd.load(e);
        return(true);
      }

      if (sa.length != 3){ // error
        System.out.println("Expecting l or l gen ind");
        return(true);
      }

// prepare to parse
      int gen,ind; // generation # and individual #
      try{
        gen=Integer.parseInt(sa[1]);
        ind=Integer.parseInt(sa[2]);
      }catch(Exception e){
        System.out.println("Parse error: l " + sa[1] + " " + sa[2]);
        return(true);
      }
// open the file and scan
      try{
        fileScanner=new Scanner(new File(fileName));
        System.out.println("Searching " + fileName);
        thisInd=load(fileScanner,gen,ind,e); // scan file and load this individual
        e.clearAllTubes();
        thisInd.load(e); // load into system
// note that the karma etc. will be setup as the file is scanned
        fileScanner.close();
      }catch(Exception e){
        System.out.println("Can't open " + fileName);
        return(true);
      }
    }

    return(true);

  }

  Genome load(Scanner sc, int tgtGen, int tgtInd, EEXIST e)
  {
    Genome ret=new Genome();
    while (sc.hasNextLine()){
      String temp=sc.nextLine();
// comment
      if (temp.charAt(0)=='*') continue; // comment
// setup
      if (temp.charAt(0)=='S'){ // setup EEXIST
        String[] toks=temp.split(",");
        double karma=Double.parseDouble(toks[1]);
        //e.setKarma(karma); // adjust this as we scan the file
//%%%
        //e.setKarma(0.25); // fixed!
        continue;
      }
// individual
      if (temp.charAt(0)=='I'){
        String[] toks=temp.split(",");
        int gen=Integer.parseInt(toks[1]);
        int ind=Integer.parseInt(toks[2]);
        double score=Double.parseDouble(toks[3]);
        double score2=Double.parseDouble(toks[4]);
        if ((gen != tgtGen)||(ind != tgtInd)) continue;
// found the individual here
        int i=5; // index in toks[] array
        for (int x=0;x<Genome.GenomeSize;x++){
          for (int y=0;y<Genome.GenomeSize;y++){
            ret.genes[x][y].SX=Double.parseDouble(toks[i++]);
            ret.genes[x][y].SY=Double.parseDouble(toks[i++]);
            ret.genes[x][y].DX=Double.parseDouble(toks[i++]);
            ret.genes[x][y].DY=Double.parseDouble(toks[i++]);
          }
        }
        System.out.println("Score=" + score + "," + score2);
        sc.close(); // done with file for now
        return(ret); // return the genome
      }
// keep reading
    } // EOF
    sc.close();
    System.out.println("WARNING: Didn't find gen " + tgtGen +
                       ", Individual " + tgtInd);
    return(null);
  }

// open the given test file (0=math, 1=Mozart, 2=Spongebob)
// and test on samples in the given range
// Each sample consists of 10 readings
  void singleTest(int test, int sample, EEXIST e)
  {
    double score=0; // running score
    double[][] data=new double[8][1]; // full set of wave data (8 bands)
    loadData(test,data); // populate it
// this loads everything in the file
    normalize(data); // scale from 0-1

// tell user how many records are available
    for (int band=0;band<8;band++){
      System.out.println("Band " + bandNames[band] + ": # records=" + data[band].length);
    }

    e.clearAllTubes();

// load in 8 strips of 40x2(constant across the vertical 2) (320x16), with a blank strip in between
// input from (36,0)-(40,4) (Math); (36,8)-(40,12) (Mozart); and (36,16)-(40,20) (SpongeBob)

    for (int band=0;band<8;band++){
      for (int i=0;i<10;i++){ // read 10 sets of readings
        double chem=data[band][10*sample+i]*40; // offset by 10*sample #
                                               // and scale from 0-40
        for (int dx=0;dx<2;dx++){
          for (int dy=0;dy<4;dy++){
            loadChems(2*i+dx,4*band+dy,chem);
          }
        }
      }
    }
// all done!

    return;
  }

// data normalizer
  void normalize(double[][]data)
  {
    for (int band=0;band<8;band++){
      double max=0;
      for (int i=0;i<data[band].length;i++){
        if (data[band][i]>max) max=data[band][i];
      }
      for (int i=0;i<data[band].length;i++){
        data[band][i]=data[band][i]/max;
      }
    }
  }

// Raw data read
 void loadData(int test,double[][]data)
  {
    double temp[]=new double[50000];
    int tempLen=0;

    tempLen=loadSingleBand(test,temp,"WDEL");saveBand(data,DEL,temp,tempLen); // copy
    tempLen=loadSingleBand(test,temp,"WTHE");saveBand(data,THE,temp,tempLen);
    tempLen=loadSingleBand(test,temp,"WLAL");saveBand(data,LAL,temp,tempLen);
    tempLen=loadSingleBand(test,temp,"WHAL");saveBand(data,HAL,temp,tempLen);
    tempLen=loadSingleBand(test,temp,"WLBE");saveBand(data,LBE,temp,tempLen);
    tempLen=loadSingleBand(test,temp,"WHBE");saveBand(data,HBE,temp,tempLen);
    tempLen=loadSingleBand(test,temp,"WLGA");saveBand(data,LGA,temp,tempLen);
    tempLen=loadSingleBand(test,temp,"WMGA");saveBand(data,MGA,temp,tempLen);
  }

  void saveBand(double[][]data, int band, double[]temp, int tempLen)
  {
    data[band]=new double[tempLen];
    for (int i=0;i<tempLen;i++) data[band][i]=temp[i];
  }

  int loadSingleBand(int test, double[] temp, String bandName)
  {
    int tempLen=0;
    String filename="BADDATA";
    Scanner sc=null;

    if (test==0) filename="math.waves";
    else if (test==1) filename="mozart.waves";
    else if (test==2) filename="spongebob.waves";
    try{
      sc=new Scanner(new File("braindata/" + filename));
    } catch(Exception e){
      System.out.println("Can't open braindata/"+filename+": "+e);
      System.exit(0);
    }
// now read the actual data
    while (sc.hasNextLine()){
      String buffer=sc.nextLine();
      String[] toks=buffer.split(" ");
      if (toks[0].equals(bandName)) temp[tempLen++]=Integer.parseInt(toks[1]);
    }
    sc.close();
    return(tempLen);
  }

/************************************
 * chemical etc read and write
 ************************************/


// load a square region of chemicals (same value throughout!)
  void loadChems(int x, int y, double chem)
  {
    e.setTube(x,y,EEXIST.SRCX,chem);
    e.setTube(x,y,EEXIST.SRCY,chem+5);
    e.setTube(x,y,EEXIST.DSTX,chem-7);
    e.setTube(x,y,EEXIST.DSTY,chem/3); // random stuff!
  }
}
