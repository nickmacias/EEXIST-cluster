class Test // FFT tester
{
  public static void main(String[] args)
  {
    double data[]=new double[5000];
    double freq=1; // 2 cycles per second
                   // t is 1/100ths of a sec
    for (double t=0;t<5000;t++){
      data[(int)t]=50*Math.cos(2*Math.PI*freq*t/100); 
    }

    double[] result;
    result=FFT.doFFT(data);

    for (int i=0;i<result.length;i++){
      if (result[i] > .002) System.out.println("["+i+"]:"+result[i]);
    }
  }
}
