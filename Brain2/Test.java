import java.util.Scanner;
import java.io.File;

class Test // FFT tester
{
  public static void main(String[] args)
  {
    double data[]=new double[5000];
    Scanner sc;
    try{
      sc=new Scanner(new File("braindata/spongebob.dat"));
    } catch(Exception e){
      System.out.println("Can't open file: " + e);
      return;
    }

    int index=0;
    int skip=10000;
    while (index < 5000){
      String buffer=sc.nextLine();
      String[] pbuf=buffer.split(" ");
      if (!pbuf[0].equals("R")) continue; // not a piece of raw data
      if (skip>=0){skip--;continue;}
      int val=Integer.parseInt(pbuf[1]);
      data[index++]=val/1000.;
    }
    sc.close();

    double[] result;
    result=FFT.doFFT(data);

    for (int i=0;i<result.length;i++){
      if (result[i] > .002) System.out.println("["+i+"]:"+result[i]);
    }
  }
}
