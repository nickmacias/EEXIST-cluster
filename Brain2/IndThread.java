import com.cellmatrix.EEXIST.API_V2.*;
import java.io.PrintWriter;
import java.awt.image.BufferedImage;
import javax.imageio.ImageIO;
import java.io.File;
import java.util.Scanner;

/***  
INIT:
Create 25 threads

MAIN LOOP (one generation)
 each thread, set inUse=false, done=false
 loop 50x:
   look for thread with inUse=false
   if done=true, read and save ID/score
   set inUse=true, done=false
   Load genome
   start()

(after 50x)
 Wait for all threads to be done
 sort, select, breed, mutate

 Back to MAIN LOOP
 ***/

// Thread for one individual
public class IndThread extends Thread{
  EEXIST e;
  Display disp;
  //boolean headless=true;
  boolean headless=false;
  boolean done=false;
  boolean inUse=false;
  Genome me=null;
  double finalScore=0;
  int id=0;
  public boolean phase2=false;
// types of waves
  int DEL=0;
  int THE=1;
  int LAL=2;
  int HAL=3;
  int LBE=4;
  int HBE=5;
  int LGA=6;
  int MGA=7;

  //public double passedCount=0;
  boolean lastTestPassed=false; // evil globals!

  boolean DEBUG=false;

  //public boolean calculateMaxScore=false;
  //public double maxScore=0; // calculate this if above is true

// access
  public boolean isDone(){return(done);}
  public boolean isInUse(){return(inUse);}
  public void setInUse(){inUse=true;} // Core should set before calling start()...
  public void clearDone(){done=false;}
  public void setGenome(Genome g){me=g;}
  public double getScore(){return(finalScore);}
  public void setID(int id){this.id=id;}
  public int getID(){return(id);}

/************************************
 * main thread
 ************************************/

  public void run()
  {
    if (System.getenv("VT").equals("VT")){
      headless=true;
    }
    if (!inUse){ // safety dance
      System.out.println("Need to set inUse before calling start() from Core\n");
      System.exit(1);
    }

    e=new EEXIST(0,40,.125,0,40,.125);
    e.proportionalFlow(true);
    //e.debug=true;
    e.setKarma(2); // !!!
    if (!headless){
      disp=new Display(e);     // create a new display
      disp.setVisible(true);  // show the display
      //ControlPanel cp=new ControlPanel("Control Panel");
      //cp.connect(this, ControlPanel.Slider,0,2,0,400,1,"Karma","cpHandler");
    }

    finalScore=0.; // additive
    for (int test=0;test<=2;test++){ // 0=math, 1=Mozart, 2=SpongeBob
     for (int sample=0;sample<=2;sample++){ // load this sample into EEXIST
      me.load(e); // load my genome
      double thisScore=assess(test, sample, e); // run a test, tally the score
      if (DEBUG) System.out.println("test=" + test + " sample=" + sample + " score=" + thisScore);
      System.out.println("id=" + id + "test=" + test + " sample=" + sample + " score=" + thisScore);
      finalScore+=thisScore; // add the scores (0+40 same as 20+20)
     }
    }
    done=true; // set this first!
    inUse=false;
  }
  
/************************************
 * actual test code
 ************************************/

  double assess(int test, int sample, EEXIST e) // assess this individual
  {
    double score=0; // running score
    double[][] data=new double[8][1]; // this is the bandwave data
    loadData(test,data); // populate it
// data is a 2D array: data[DEL] is the delta wave data, for example
    normalize(data); // scale from 0-1

    e.clearAllTubes();

// LOAD CHEMICALS

// 8 bands, each 4 tubes tall
// each sample is 2 tubes wide (max of 160 samples=320 tubes=coord of 40)
// use int coords
    for (int band=0;band<8;band++){
      for (int i=0;i<10;i++){ // read 10 sets of wave data
        double chem=data[band][10*sample+i]*40; // offset by sample #
        for (int dx=0;dx<2;dx++){
          for (int dy=0;dy<4;dy++){
            loadChems(2*i+dx,4*band+dy,chem);
          }
        }
      }
    }

    // step 250 ticks, then read value for 20 more
    for (int i=0;i<250;i++){
      e.step(1);  // pre-process/stabilize
    }

// sample outputs
// read output from:
//                  (0,36)-(4,40) (Math)
//                  (8,36)-(12,40) (Mozart)
//                  (16,36)-(20,40) (Spongebob)

    for (int i=0;i<20;i++){ 
      e.step(1);
      double s0=readChems(0,36,4,40);
      double s1=readChems(8,36,12,40);
      double s2=readChems(16,36,20,40);

// find max
      double max=0;int maxsub=(-1);
      if (s0 > max){max=s0;maxsub=0;}
      if (s1 > max){max=s1;maxsub=1;}
      if (s2 > max){max=s2;maxsub=2;}

      if (test==maxsub) ++score; // :)
    } // end of tests
    return(score); // max=# of iterations
  }

// data normalizer
  void normalize(double[][]data)
  {
    for (int band=0;band<8;band++){
      double max=0;
      for (int i=0;i<data[band].length;i++){
        if (data[band][i]>max) max=data[band][i];
      }
      for (int i=0;i<data[band].length;i++){
        data[band][i]=data[band][i]/max;
      }
    }
  }

// Raw data read
  void loadData(int test,double[][]data)
  {
    double temp[]=new double[50000];
    int tempLen=0;

    tempLen=loadSingleBand(test,temp,"WDEL");saveBand(data,DEL,temp,tempLen); // copy
    tempLen=loadSingleBand(test,temp,"WTHE");saveBand(data,THE,temp,tempLen);
    tempLen=loadSingleBand(test,temp,"WLAL");saveBand(data,LAL,temp,tempLen);
    tempLen=loadSingleBand(test,temp,"WHAL");saveBand(data,HAL,temp,tempLen);
    tempLen=loadSingleBand(test,temp,"WLBE");saveBand(data,LBE,temp,tempLen);
    tempLen=loadSingleBand(test,temp,"WHBE");saveBand(data,HBE,temp,tempLen);
    tempLen=loadSingleBand(test,temp,"WLGA");saveBand(data,LGA,temp,tempLen);
    tempLen=loadSingleBand(test,temp,"WMGA");saveBand(data,MGA,temp,tempLen);
  }

  void saveBand(double[][]data, int band, double[]temp, int tempLen)
  {
    data[band]=new double[tempLen];
    for (int i=0;i<tempLen;i++) data[band][i]=temp[i];
  }

  int loadSingleBand(int test, double[] temp, String bandName)
  {
    int tempLen=0;
    String filename="BADDATA";
    Scanner sc=null;

    if (test==0) filename="math.waves";
    else if (test==1) filename="mozart.waves";
    else if (test==2) filename="spongebob.waves";
    try{
      sc=new Scanner(new File("braindata/" + filename));
    } catch(Exception e){
      System.out.println("Can't open braindata/"+filename+": "+e);
      System.exit(0);
    }
// now read the actual data
    while (sc.hasNextLine()){
      String buffer=sc.nextLine();
      String[] toks=buffer.split(" ");
      if (toks[0].equals(bandName)) temp[tempLen++]=Integer.parseInt(toks[1]);
    }
    sc.close();
    return(tempLen);
  }

/************************************
 * chemical etc read and write
 ************************************/

// read chems in a region and tally correct ones (y shows correct value)
  double readChems(double startX,double startY,double endX,double endY)
  {
    int tally=0;
    double sum=0; // just add all chemical levels within given region
    for (double x=startX;x<endX;x+=e.getDx()){
      for (double y=startY;y<endY;y+=e.getDy()){
        sum+=e.getTube(x,y,EEXIST.SRCX) + e.getTube(x,y,EEXIST.SRCY) +
             e.getTube(x,y,EEXIST.DSTX) + e.getTube(x,y,EEXIST.DSTY);
      }
    }
    return(sum); // use this for choosing survivors etc.
  }

// load a square region of chemicals (same value throughout!)
  void loadChems(int x, int y, double chem)
  {
    e.setTube(x,y,EEXIST.SRCX,chem);
    e.setTube(x,y,EEXIST.SRCY,chem+5);
    e.setTube(x,y,EEXIST.DSTX,chem-7);
    e.setTube(x,y,EEXIST.DSTY,chem/3); // random stuff!
  }

/************************************
 * legacy code...
 ************************************/

  public void cpHandler(ControlArgs arg)
  {
    switch(arg.getUserID()){
    case 1: // karma
      //e.setKarma(((double)arg.getSliderPos())/10);
      //writeStatus(pw,e); // write new EEXIST data
      //System.out.println("\n\nKARMA SET TO " + e.getKarma() + "\n");
      break;
    }
  }

}
