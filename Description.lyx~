#LyX 2.2 created this file. For more info see http://www.lyx.org/
\lyxformat 508
\begin_document
\begin_header
\save_transient_properties true
\origin unavailable
\textclass article
\use_default_options true
\maintain_unincluded_children false
\language english
\language_package default
\inputencoding auto
\fontencoding global
\font_roman "default" "default"
\font_sans "default" "default"
\font_typewriter "default" "default"
\font_math "auto" "auto"
\font_default_family default
\use_non_tex_fonts false
\font_sc false
\font_osf false
\font_sf_scale 100 100
\font_tt_scale 100 100
\graphics default
\default_output_format default
\output_sync 0
\bibtex_command default
\index_command default
\paperfontsize default
\spacing single
\use_hyperref false
\papersize default
\use_geometry true
\use_package amsmath 1
\use_package amssymb 1
\use_package cancel 1
\use_package esint 1
\use_package mathdots 1
\use_package mathtools 1
\use_package mhchem 1
\use_package stackrel 1
\use_package stmaryrd 1
\use_package undertilde 1
\cite_engine basic
\cite_engine_type default
\biblio_style plain
\use_bibtopic false
\use_indices false
\paperorientation landscape
\suppress_date false
\justification true
\use_refstyle 1
\index Index
\shortcut idx
\color #008000
\end_index
\leftmargin 0cm
\rightmargin 0cm
\secnumdepth 3
\tocdepth 3
\paragraph_separation indent
\paragraph_indentation default
\quotes_language english
\papercolumns 1
\papersides 1
\paperpagestyle default
\tracking_changes false
\output_changes false
\html_math_output 0
\html_css_as_file 0
\html_be_strict false
\end_header

\begin_body

\begin_layout Standard
\begin_inset Formula $S_{x}(X,Y,T)=\intop_{0}^{T}\intop_{-\infty}^{\infty}\intop_{-\infty}^{\infty}S_{x}(X,Y,t)(k(x,y,D_{x}(x,y,t)+BD_{x}(x,y),D_{y}(x,y,t)+BD_{y}(x,y))-k(x,y,S_{x}(x,y,t)+BS{}_{x}(x,y),S_{y}(x,y,t)+BS{}_{y}(x,y)))dxdydt$
\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula $S_{y}(X,Y,T)=\intop_{0}^{T}\intop_{-\infty}^{\infty}\intop_{-\infty}^{\infty}S_{y}(X,Y,t)(k(x,y,D_{x}(x,y,t)+BD_{x}(x,y),D_{y}(x,y,t)+BD_{y}(x,y))-k(x,y,S_{x}(x,y,t)+BS{}_{x}(x,y),S_{y}(x,y,t)+BS{}_{y}(x,y)))dxdydt$
\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula $D_{x}(X,Y,T)=\intop_{0}^{T}\intop_{-\infty}^{\infty}\intop_{-\infty}^{\infty}D_{x}(X,Y,t)(k(x,y,D_{x}(x,y,t)+BD_{x}(x,y),D_{y}(x,y,t)+BD_{y}(x,y))-k(x,y,S_{x}(x,y,t)+BS{}_{x}(x,y),S_{y}(x,y,t)+BS{}_{y}(x,y)))dxdydt$
\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula $D_{y}(X,Y,T)=\intop_{0}^{T}\intop_{-\infty}^{\infty}\intop_{-\infty}^{\infty}D{}_{y}(X,Y,t)(k(x,y,D_{x}(x,y,t)+BD_{x}(x,y),D_{y}(x,y,t)+BD_{y}(x,y))-k(x,y,S_{x}(x,y,t)+BS{}_{x}(x,y),S_{y}(x,y,t)+BS{}_{y}(x,y)))dxdydt$
\end_inset


\end_layout

\begin_layout Standard
where
\end_layout

\begin_layout Standard
\begin_inset Formula $k(x_{0},y_{0,}x,y)$
\end_inset

is the karma from 
\begin_inset Formula $(x,y)$
\end_inset

to 
\begin_inset Formula $(x_{0},y_{0})$
\end_inset

given by
\end_layout

\begin_layout Standard
\begin_inset Formula $k(x_{0},y_{0},x,y)=e^{(-\frac{\sqrt{(x-x_{0})^{2}+(y-y_{0})^{2}}}{K})}$
\end_inset

 where 
\begin_inset Formula $K$
\end_inset

is the global karma;
\end_layout

\begin_layout Standard
\begin_inset Formula $S_{x}(X,Y,T)$
\end_inset

is the amount of Source X chemical at coordinates 
\begin_inset Formula $(X,Y)$
\end_inset

at time 
\begin_inset Formula $T$
\end_inset

;
\end_layout

\begin_layout Standard
\begin_inset Formula $S_{y}(X,Y,T)$
\end_inset

is the amount of Source Y chemical at coordinates 
\begin_inset Formula $(X,Y)$
\end_inset

at time 
\begin_inset Formula $T$
\end_inset

;
\end_layout

\begin_layout Standard
\begin_inset Formula $D_{x}(X,Y,T)$
\end_inset

is the amount of Destination X chemical at coordinates 
\begin_inset Formula $(X,Y)$
\end_inset

at time 
\begin_inset Formula $T$
\end_inset

;
\end_layout

\begin_layout Standard
\begin_inset Formula $D_{x}(X,Y,T)$
\end_inset

is the amount of Destination Y chemical at coordinates 
\begin_inset Formula $(X,Y)$
\end_inset

at time 
\begin_inset Formula $T$
\end_inset

;
\end_layout

\begin_layout Standard
\begin_inset Formula $BS_{x}(x,y)$
\end_inset

 is the Source X bias at coordinates 
\begin_inset Formula $(x,y)$
\end_inset

;
\end_layout

\begin_layout Standard
\begin_inset Formula $BS_{y}(x,y)$
\end_inset

 is the Source Y bias at coordinates 
\begin_inset Formula $(x,y)$
\end_inset

;
\end_layout

\begin_layout Standard
\begin_inset Formula $BD_{x}(x,y)$
\end_inset

 is the Destination X bias at coordinates 
\begin_inset Formula $(x,y)$
\end_inset

; and
\end_layout

\begin_layout Standard
\begin_inset Formula $BD_{y}(x,y)$
\end_inset

 is the Destination Y bias at coordinates 
\begin_inset Formula $(x,y)$
\end_inset

.
\end_layout

\end_body
\end_document
