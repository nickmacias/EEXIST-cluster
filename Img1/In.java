import java.awt.image.BufferedImage;
import javax.imageio.ImageIO;
import java.io.File;

class In
{
  public static void main(String[] args)
  {
    BufferedImage img=null;
    try{
      //img=ImageIO.read(new File("test.jpg"));
      //img=ImageIO.read(new File("test.png"));
      //img=ImageIO.read(new File("test.gif"));
      img=ImageIO.read(new File("yalefaces/subject02.centerlight"));
    } catch (Exception e){
      System.out.println("Error: " + e);
      return;
    }

// dump pixels
    for (int y=0;y<243;y+=2){
      for (int x=0;x<320;x++){
        int c=img.getRGB(x,y);
        char cc;
        int i=(c&0xff) + ((c>>8)&0xff) + ((c>>16)&0xff);
        i=i/3;
        if (i>0xc0) cc='@';
        else if (i > 0x80) cc='0';
        else if (i > 0x40) cc='.';
        else cc=' ';
        System.out.print(cc);
      }
      System.out.println();
    }
  }
}
