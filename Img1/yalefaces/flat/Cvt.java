import java.awt.image.BufferedImage;
import javax.imageio.ImageIO;
import java.io.File;
import java.io.PrintWriter;

// convert jpg to flatfile
class Cvt
{
  public static void main(String[] args)
  {
    PrintWriter pw; // output stream
    String fin,fout;
    fin=args[0];
    fout=fin+".txt";
    System.out.println("Converting " + fin + " to " + fout);

    BufferedImage img=null;
    try{
      img=ImageIO.read(new File(fin));
      pw=new PrintWriter(fout);
    } catch (Exception e){
      System.out.println("Error: " + e);
      return;
    }

    pw.println("320 243"); // dims

// dump pixels
    for (int y=0;y<243;y++){
      for (int x=0;x<320;x++){
        int c=img.getRGB(x,y);
        pw.print(Integer.toHexString(c) + " ");
      }
      pw.println();
    }
    pw.close();
  }
}
