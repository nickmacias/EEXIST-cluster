import com.cellmatrix.EEXIST.API_V2.*;

/*
 * Genome of an organism
 * This is comprised of a matrix of genes, plus
 * a set of methods for manipulating the genome
 */
public class Genome {
  static int GenomeSize=9; // 1+# of genes in the genome *per dimension*
  int xLen,yLen; // # tubes per gene

  
  public Gene[][] genes=new Gene[Genome.GenomeSize][Genome.GenomeSize];
// genes[i][j] is upper-left corner of gene [i][j]
// for 8x8,want e.g. [8][8] for LR corner
  // just construct each gene
  Genome()
  {
    for (int x=0;x<GenomeSize;x++){
      for (int y=0;y<GenomeSize;y++){
        genes[x][y]=new Gene(); // everything should be initialized to 0...
      }
    }
  }

  void load(EEXIST e) // load the genome into the machine
  {
    xLen=e.numXBins()/(GenomeSize-1);
    yLen=e.numYBins()/(GenomeSize-1); // # of tubes per genome

// zero-out the system's chemicals
    e.clearAllTubes();

// DO IT! HAHAHAHAHAHA!

    for (int y=0;y<e.numYBins();y++){
      for (int x=0;x<e.numXBins();x++){

// first find corner indicies
        int xBin0=x/xLen;
        int yBin0=y/yLen; // these run from 0-8 (8 at the right/bottom edges)
        int xBin1=xBin0;
        int yBin1=yBin0;
        if (xBin1 < GenomeSize-1) ++xBin1; 
        if (yBin1 < GenomeSize-1) ++yBin1; // these mark the corners now

// find where (x,y) sits inside this region (so from 0-1)
        double xx,yy;
        xx=((double)x-((double)xLen*(double)xBin0))/(double)xLen;
        yy=((double)y-((double)yLen*(double)yBin0))/(double)yLen;

        double B00,B01,B10,B11,Bt,Bb,B;

// now find biases
        B00=genes[xBin0][yBin0].SX; // upper-left
        B01=genes[xBin1][yBin0].SX; // upper-right
        B10=genes[xBin0][yBin1].SX; // lower-left
        B11=genes[xBin1][yBin1].SX; // lower-right
        Bt=B00+xx*(B01-B00); // bias along top edge
        Bb=B10+xx*(B11-B10); // bias along bottom edge
        B=Bt+yy*(Bb-Bt); // final bias!
        e.setBias(x,y, EEXIST.SRCX, B); // load into system
//System.out.println("x,y=" + x + ", " + y + "   xx,yy=" + xx + "," + yy + "    B00 B01 B10 B11=" + B00 + " " +B01 + " " + B10 + " " + B11 + "    Bt Bb b=" + Bt + " " + Bb + " " + B);

        B00=genes[xBin0][yBin0].SY;
        B01=genes[xBin1][yBin0].SY;
        B10=genes[xBin0][yBin1].SY;
        B11=genes[xBin1][yBin1].SY;
        Bt=B00+xx*(B01-B00);
        Bb=B10+xx*(B11-B10);
        B=Bt+yy*(Bb-Bt);
        e.setBias(x,y, EEXIST.SRCY, B);

        B00=genes[xBin0][yBin0].DX;
        B01=genes[xBin1][yBin0].DX;
        B10=genes[xBin0][yBin1].DX;
        B11=genes[xBin1][yBin1].DX;
        Bt=B00+xx*(B01-B00);
        Bb=B10+xx*(B11-B10);
        B=Bt+yy*(Bb-Bt);
        e.setBias(x,y, EEXIST.DSTX, B);

        B00=genes[xBin0][yBin0].DY;
        B01=genes[xBin1][yBin0].DY;
        B10=genes[xBin0][yBin1].DY;
        B11=genes[xBin1][yBin1].DY;
        Bt=B00+xx*(B01-B00);
        Bb=B10+xx*(B11-B10);
        B=Bt+yy*(Bb-Bt);
        e.setBias(x,y, EEXIST.DSTY, B);

      }
    }
  }

  Genome copy() // make a copy
  {
    Genome temp=new Genome();
    for (int x=0;x<GenomeSize;x++){
      for (int y=0;y<GenomeSize;y++){
        temp.genes[x][y]=genes[x][y].copy();
      }
    }
    return(temp);
  }

// tweak current genome's genes
  void mutate(double amount)
  {
    for (int x=0;x<GenomeSize;x++){
      for (int y=0;y<GenomeSize;y++){
        if (Math.random() < amount) genes[x][y].SX*=(.5+Math.random()); // .x5-1.5x
        if (Math.random() < amount) genes[x][y].SY*=(.5+Math.random()); // .x5-1.5x
        if (Math.random() < amount) genes[x][y].DX*=(.5+Math.random()); // .x5-1.5x
        if (Math.random() < amount) genes[x][y].DY*=(.5+Math.random()); // .x5-1.5x
      }
    }
  }

// make with a given genome
  void merge(Genome g)
  {
    for (int x=0;x<GenomeSize;x++){
      for (int y=0;y<GenomeSize;y++){
      // TODO may want to actually merge (average) the 2 genes
        genes[x][y].DX=(genes[x][y].DX+g.genes[x][y].DX)/2.;
        genes[x][y].DY=(genes[x][y].DY+g.genes[x][y].DY)/2.;
        genes[x][y].SX=(genes[x][y].SX+g.genes[x][y].SX)/2.;
        genes[x][y].SY=(genes[x][y].SY+g.genes[x][y].SY)/2.;
      }
    }
  }

  void randomize()
  {
// randomize each gene
    for (int x=0;x<GenomeSize;x++){
      for (int y=0;y<GenomeSize;y++){
        genes[x][y].randomize();
      }
    }
  }
}
