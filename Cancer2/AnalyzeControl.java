//
// control panel class
//

import com.cellmatrix.EEXIST.API_V2.*;
import java.awt.EventQueue;
import java.util.Scanner;
import java.io.File;
import java.awt.image.BufferedImage;
import javax.imageio.ImageIO;

public class AnalyzeControl extends Thread{
  EEXIST e;
  Display disp;
  AnalyzeCore core=null; // create this when we want to run forward

  int NUMBEROFSTEPS=250;
  double KARMA=3.0;

  String fileName;
  Scanner fileScanner=null; // for reading from raw file

  int delayTime=0; // pass this to core before running a test

  Genome thisInd=null; // loaded from file

// main method: construct EEXIST, display and control panel
// then idle until user starts thread

  public void run()
  {
    e=new EEXIST(0,40,.125,0,40,.125); // main EEXIST object
    e.proportionalFlow(true);
    e.setKarma(KARMA);

    disp=new Display(e); // create a new display
    disp.setVisible(true);  // show the display

    ControlPanel cp=new ControlPanel("Control Panel");
    //cp.connect(this,ControlPanel.Slider,0,10,0,400,1,"Karma","cpHandler");
    //cp.connect(this,ControlPanel.CheckBox,0,10,"Run/Pause","cpHandler");
    cp.connect(this,ControlPanel.TextIn,0,11,"Command","cpHandler");
  }
// gene.load(e);e.step(1);
  
  public void cpHandler(ControlArgs arg)
  {
    switch(arg.getUserID()){
    case 1: // karma
      //e.setKarma(((double)arg.getSliderPos())/10);
      System.out.println("\n\nKARMA SET TO " + e.getKarma() + "\n");
      break;
    case 10: // Run/Pause
      //System.out.println(arg.getCheckState()?"Running":"Paused");
      break;
    case 11: // text input
      System.out.println("<"+arg.getTextIn()+">");
      if (!parse(arg.getTextIn())) System.exit(0); // returns FALSE after Q command
      break;
    }
  }

  boolean parse(String s)
//
// r - run forever
// r n - run n steps
// h - halt
// f filename - name input file
// l gen ind - load this individual
// l - reload last individual
// t b0 b1 - run a test!
// x - load a random tube (debug gadget)
// Q - quit all
//
  {
    String[] sa=s.split(" ");
    for (int i=0;i<sa.length;i++){
      System.out.println("["+i+"]:<"+sa[i]+">");
    }
    if (sa.length < 1) return(true); // just an ENTER?

// QUIT
    if (sa[0].equals("Q")){
      if (core!=null) core.halt();
      return(false);
    }

// RUN commands
    if (sa[0].equals("h")){
      if (core != null){
        core.halt(); // signal core to exit
        core=null; // and remember that it's gone
        return(true);
      }
    }
    if (sa[0].equals("r")){
      if (core==null){
        core=new AnalyzeCore(e,disp);
      }
// is a length specified?
      if (sa.length==2){ // yes
        try{
          core.setLimit(Integer.parseInt(sa[1]));
        }catch(Exception e){
          System.out.println("Error parsing " + sa[1]);return(true);
        }
        core.start(); // start the thread
        core=null; // since this thread will exit shortly
      } else {
        core.setLimit(0); // no limit
        core.start(); // start the core thread; will exit after HALT of when done
      }
    }

// set delay
    if (sa[0].equals("d")){
      if (sa.length==2){ // yes
        delayTime=Integer.parseInt(sa[1]);
        System.out.println("Delay set to " + delayTime);
      }
      return(true);
    }

// test :)
    if (sa[0].equals("t")){ // t subject pose
      if (sa.length == 3){ // okay :)
// get subject and pose
        int subject=Integer.parseInt(sa[1]);
        int pose=Integer.parseInt(sa[2]);
        singleTest(subject,pose,e); // load these inputs
// now tell the core system to tally the output, and run 50 steps
        if (core==null){
          core=new AnalyzeCore(e,disp);
        }
        core.running=true;
        core.setLimit(NUMBEROFSTEPS);
        core.setDelayTime(delayTime);
        core.checkOutput(true); // tally and output score at end
        core.start();
/***
        while (core.running){
          try{
            Thread.sleep(100);
          } catch(Exception e){
          }
        }
***/
        core=null;
        return(true);
      } // end of test

    } // end of "t"


// file
    if (sa[0].equals("f")){
      if (sa.length<2) return(true); // need a filename
      fileName=sa[1]; // save this
      try{
        fileScanner=new Scanner(new File(fileName));
        System.out.println(fileName + " opened succesfully for reading");
        fileScanner.close();
      }catch(Exception e){
        System.out.println("ERROR: Can't open " + sa[1]);
      }
      return(true);
    }

// random
    if (sa[0].equals("x")){
      e.setTube(40*Math.random(),40*Math.random(),(int)(2.*Math.random()),40.*Math.random());
      return(true);
    }

// load an individual
    if (sa[0].equals("l")){ // check usage
      if (sa.length==1){ // load last individual
        e.clearAllTubes();
        thisInd.load(e);
        return(true);
      }

      if (sa.length != 3){ // error
        System.out.println("Expecting l or l gen ind");
        return(true);
      }

// prepare to parse
      int gen,ind; // generation # and individual #
      try{
        gen=Integer.parseInt(sa[1]);
        ind=Integer.parseInt(sa[2]);
      }catch(Exception e){
        System.out.println("Parse error: l " + sa[1] + " " + sa[2]);
        return(true);
      }
// open the file and scan
      try{
        fileScanner=new Scanner(new File(fileName));
        System.out.println("Searching " + fileName);
        thisInd=load(fileScanner,gen,ind,e); // scan file and load this individual
        e.clearAllTubes();
        thisInd.load(e); // load into system
// note that the karma etc. will be setup as the file is scanned
        fileScanner.close();
      }catch(Exception e){
        System.out.println("Can't open " + fileName);
        return(true);
      }
    }

    return(true);

  }

  Genome load(Scanner sc, int tgtGen, int tgtInd, EEXIST e)
  {
    Genome ret=new Genome();
    while (sc.hasNextLine()){
      String temp=sc.nextLine();
// comment
      if (temp.charAt(0)=='*') continue; // comment
// setup
      if (temp.charAt(0)=='S'){ // setup EEXIST
        String[] toks=temp.split(",");
        double karma=Double.parseDouble(toks[1]);
        //e.setKarma(karma); // adjust this as we scan the file
//%%%
        //e.setKarma(0.25); // fixed!
        continue;
      }
// individual
      if (temp.charAt(0)=='I'){
        String[] toks=temp.split(",");
        int gen=Integer.parseInt(toks[1]);
        int ind=Integer.parseInt(toks[2]);
        double score=Double.parseDouble(toks[3]);
        double score2=Double.parseDouble(toks[4]);
        if ((gen != tgtGen)||(ind != tgtInd)) continue;
// found the individual here
        int i=5; // index in toks[] array
        for (int x=0;x<Genome.GenomeSize;x++){
          for (int y=0;y<Genome.GenomeSize;y++){
            ret.genes[x][y].SX=Double.parseDouble(toks[i++]);
            ret.genes[x][y].SY=Double.parseDouble(toks[i++]);
            ret.genes[x][y].DX=Double.parseDouble(toks[i++]);
            ret.genes[x][y].DY=Double.parseDouble(toks[i++]);
          }
        }
        System.out.println("Score=" + score + "," + score2);
        sc.close(); // done with file for now
        return(ret); // return the genome
      }
// keep reading
    } // EOF
    sc.close();
    System.out.println("WARNING: Didn't find gen " + tgtGen +
                       ", Individual " + tgtInd);
    return(null);
  }

// load given subject, pose into e
  void singleTest(int subject, int pose, EEXIST e)
  {
// filenames: yalefaces/subject##.suffix
    String[] suffixes={
              "glasses",
              "happy",
              "leftlight",
              "noglasses",
              "normal",
              "rightlight",
              "sad",
              "sleepy",
              "surprised",
              "wink"
             };
// build filename corresponding to subject and pose
    String filename="yalefaces/subject" + ((subject>9)?"":"0") +
                     subject+"."+suffixes[pose];

// load this image into EEXIST (e)
   if (true) System.out.println("Loading subject " + subject + " filename=<" + filename + ">");

    e.clearAllTubes();

    BufferedImage img=null;
    try{
      img=ImageIO.read(new File(filename));
    } catch (Exception ex){
      System.out.println("Error reading " + filename + "(subject " + subject + "): " + ex);
      return;
    }

// load chemicals
// 0->40, dx=0.125, so entire region is 320x320
// load image in top 320x243; read outputs from lower left 20x20 regions
    for (int y=0;y<243;y+=2){
      for (int x=0;x<320;x+=2){
        int c=img.getRGB(x,y); // full pixel value
        double red=(c&0xff);
        double green=((c>>8)&0xff);
        double blue=((c>>16)&0xff);
        red=red/7;green=green/7;blue=blue/7; // max ~37
        double alpha=(red+blue+green)/3.; // really this is the channel that matters
        //loadChems(x/8., y/8., red, green, blue, (red+green+blue)/3.); // rgb + alpha?
// make it visible...
        loadChems(x/16., y/16., alpha,alpha+5,alpha-7,alpha/3);
      }
    }

// all done!

    return;
  }

// load a square region of chemicals (same value throughout!)
  void loadChems(double x, double y, double v1, double v2, double v3, double v4)
  {
    e.setTube(x,y,EEXIST.SRCX,v1);
    e.setTube(x,y,EEXIST.SRCY,v2);
    e.setTube(x,y,EEXIST.DSTX,v3);
    e.setTube(x,y,EEXIST.DSTY,v4);
  }

}
