import com.cellmatrix.EEXIST.API_V2.*;
import java.io.PrintWriter;
import java.awt.image.BufferedImage;
import javax.imageio.ImageIO;
import java.io.File;
import java.util.Scanner;

/***
INIT:
Create 25 threads

MAIN LOOP (one generation)
 each thread, set inUse=false, done=false
 loop 50x:
   look for thread with inUse=false
   if done=true, read and save ID/score
   set inUse=true, done=false
   Load genome
   start()

(after 50x)
 Wait for all threads to be done
 sort, select, breed, mutate

 Back to MAIN LOOP
 ***/

// Thread for one individual
public class IndThread extends Thread{
  EEXIST e;
  Display disp;
  //boolean headless=true;
  boolean headless=false;
  boolean done=false;
  boolean inUse=false;
  Genome me=null;
  double finalScore=0;
  int id=0;
  public boolean phase2=false;

  //public double passedCount=0;
  boolean lastTestPassed=false; // evil globals!

  boolean DEBUG=false;

  //public boolean calculateMaxScore=false;
  //public double maxScore=0; // calculate this if above is true

// access
  public boolean isDone(){return(done);}
  public boolean isInUse(){return(inUse);}
  public void setInUse(){inUse=true;} // Core should set before calling start()...
  public void clearDone(){done=false;}
  public void setGenome(Genome g){me=g;}
  public double getScore(){return(finalScore);}
  public void setID(int id){this.id=id;}
  public int getID(){return(id);}

/************************************
 * main thread
 ************************************/

  public void run()
  {
    if (System.getenv("VT").equals("VT")){
      headless=true;
    }
    if (!inUse){ // safety dance
      System.out.println("Need to set inUse before calling start() from Core\n");
      System.exit(1);
    }

    e=new EEXIST(0,40,.125,0,40,.125);
    e.proportionalFlow(true);
    //e.debug=true;
    e.setKarma(2); // !!!
    if (!headless){
      disp=new Display(e);     // create a new display
      disp.setVisible(true);  // show the display
      ControlPanel cp=new ControlPanel("Control Panel");
      cp.connect(this, ControlPanel.Slider,0,2,0,400,1,"Karma","cpHandler");
    }

    finalScore=0.; // additive
    for (int test=0;test<=2;test++){ // 0=math, 1=Mozart, 2=SpongeBob
     for (int sample=0;sample<=6;sample+=2){ // load this sample into EEXIST
      me.load(e); // load my genome
      double thisScore=assess(test, sample, e); // run a test, tally the score
      if (DEBUG) System.out.println("test=" + test + " sample=" + sample + " score=" + thisScore);
      System.out.println("id=" + id + "test=" + test + " sample=" + sample + " score=" + thisScore);
      finalScore+=thisScore; // add the scores (0+40 same as 20+20)
     }
    }
    done=true; // set this first!
    inUse=false;
  }
  
/************************************
 * actual test code
 ************************************/

  double assess(int test, int sample, EEXIST e) // assess this individual
  {
    double score=0; // running score
    double[] data=new double[2560]; // this is the wave data (approx 4 sec)
    loadData(test,sample,data); // populate it
    if (getID()==0){
      for (int i=0;i<25;i++) System.out.print(data[i]+",");System.out.println(); // debug
    }

    e.clearAllTubes();

// load chemicals
// load in 8 strips of 40x2(constant across the vertical 2) (320x16), with a blank strip in between
// input from (36,0)-(40,4) (Math); (36,8)-(40,12) (Mozart); and (36,16)-(40,20) (SpongeBob)
    int index=0; // reference the data
    for (double row=0;row<32;row+=4){
      for (double x=0;x<40;x+=e.getDx()){
        double chem=(data[index++]+2048)/103.; // normalize to [0,40)
        if (chem > 40) chem=40;if (chem < 0) chem=0; // shouldn't be necessary
        loadChems(x,row,chem); // load in a column of chemicals at (x,row)-(x,row+2)
      }
    }

    // step 250 ticks, then read value for 20 more
    for (int i=0;i<250;i++){
      e.step(1);  // pre-process/stabilize
    }

// sample outputs
    for (int i=0;i<20;i++){ 
      e.step(1);
      double s0=readChems(0,36,4,40);
      double s1=readChems(8,36,12,40);
      double s2=readChems(16,36,20,40);

// find max, avoid typos :)
      double max=0;int maxsub=(-1);
      if (s0 > max){max=s0;maxsub=0;}
      if (s1 > max){max=s1;maxsub=1;}
      if (s2 > max){max=s2;maxsub=2;}

      if (test==maxsub) ++score; // :)
    } // end of tests
    return(score); // max=# of iterations
  }

// Raw data read
  void loadData(int test,int sample,double[]data)
  {
    String filename="BADDATA";
    Scanner sc=null;

    if (test==0) filename="math.dat";
    else if (test==1) filename="mozart.dat";
    else if (test==2) filename="spongebob.dat";
    try{
      sc=new Scanner(new File("braindata/" + filename));
    } catch(Exception e){
      System.out.println("Can't open braindata/"+filename+": "+e);
      System.exit(0);
    }
// skip earlier tests
    for (int i=0;i<sample;i++){
      for (int rec=0;rec<2560;rec++){
        sc.nextLine();
      }
    }
// now read the actual data
    for (int i=0;i<2560;i++){
      String buffer=sc.nextLine();
      String[] toks=buffer.split(" ");
      data[i]=Integer.parseInt(toks[1]); // save the integer value
    }
    sc.close();
  }

/************************************
 * chemical etc read and write
 ************************************/

// read chems in a region and tally correct ones (y shows correct value)
  double readChems(double startX,double startY,double endX,double endY)
  {
    int tally=0;
    double sum=0; // just add all chemical levels within given region
    for (double x=startX;x<endX;x+=e.getDx()){
      for (double y=startY;y<endY;y+=e.getDy()){
        sum+=e.getTube(x,y,EEXIST.SRCX) + e.getTube(x,y,EEXIST.SRCY) +
             e.getTube(x,y,EEXIST.DSTX) + e.getTube(x,y,EEXIST.DSTY);
      }
    }
    return(sum); // use this for choosing survivors etc.
  }

// load a square region of chemicals (same value throughout!)
  void loadChems(double x, double y, double chem)
  {
    for (double inc=0;inc<2;inc+=e.getDy()){
      e.setTube(x,y+inc,EEXIST.SRCX,chem);
      e.setTube(x,y+inc,EEXIST.SRCY,chem);
      e.setTube(x,y+inc,EEXIST.DSTX,chem);
      e.setTube(x,y+inc,EEXIST.DSTY,chem);
    }
  }

/************************************
 * legacy code...
 ************************************/

  public void cpHandler(ControlArgs arg)
  {
    switch(arg.getUserID()){
    case 1: // karma
      //e.setKarma(((double)arg.getSliderPos())/10);
      //writeStatus(pw,e); // write new EEXIST data
      System.out.println("\n\nKARMA SET TO " + e.getKarma() + "\n");
      break;
    }
  }

}
