import com.cellmatrix.EEXIST.API_V2.*;
import java.io.PrintWriter;
import java.awt.image.BufferedImage;
import javax.imageio.ImageIO;
import java.io.File;

/***
INIT:
Create 25 threads

MAIN LOOP (one generation)
 each thread, set inUse=false, done=false
 loop 50x:
   look for thread with inUse=false
   if done=true, read and save ID/score
   set inUse=true, done=false
   Load genome
   start()

(after 50x)
 Wait for all threads to be done
 sort, select, breed, mutate

 Back to MAIN LOOP
 ***/

// Thread for one individual
public class IndThread extends Thread{
  EEXIST e;
  Display disp;
  boolean headless=false;
  //boolean headless=false;
  boolean done=false;
  boolean inUse=false;
  Genome me=null;
  double finalScore=0;
  int id=0;
  public boolean phase2=false;

  //public double passedCount=0;
  boolean lastTestPassed=false; // evil globals!

  boolean DEBUG=false;

  //public boolean calculateMaxScore=false;
  //public double maxScore=0; // calculate this if above is true

// access
  public boolean isDone(){return(done);}
  public boolean isInUse(){return(inUse);}
  public void setInUse(){inUse=true;} // Core should set before calling start()...
  public void clearDone(){done=false;}
  public void setGenome(Genome g){me=g;}
  public double getScore(){return(finalScore);}
  public void setID(int id){this.id=id;}
  public int getID(){return(id);}

/************************************
 * main thread
 ************************************/

  public void run()
  {
    if (!inUse){ // safety dance
      System.out.println("Need to set inUse before calling start() from Core\n");
      System.exit(1);
    }

    e=new EEXIST(0,40,.125,0,40,.125);
    e.proportionalFlow(true);
    //e.debug=true;
    e.setKarma(.2); // !!!
    if (!headless){
      disp=new Display(e);     // create a new display
      disp.setVisible(true);  // show the display
      ControlPanel cp=new ControlPanel("Control Panel");
      cp.connect(this, ControlPanel.Slider,0,2,0,400,1,"Karma","cpHandler");
    }

    finalScore=0.; // additive
    //finalScore=1.; // multiplicative
    //passedCount=0; // let's tally how many of the tests pass
    for (int subject=1;subject<=10;subject++){
     for (int image=0;image<5;image++){ // load this image into EEXIST
      me.load(e); // load my genome
      double thisScore=assess(subject, image, e); // run a test, tally the score
      if (DEBUG) System.out.println("subject=" + subject + " image=" + image + " score=" + thisScore);
System.out.println("id=" + id + " subject=" + subject + " image=" + image + " score=" + thisScore);
      finalScore+=thisScore; // add the scores (0+40 same as 20+20)
      //thisScore=.1+(thisScore/20.); // 0.1-1.1
      //finalScore*=thisScore; // so 0/20 really hurts score!
     }
    }
    done=true; // set this first!
    inUse=false;
  }
  
/************************************
 * actual test code
 ************************************/

  double assess(int subject, int image, EEXIST e) // assess this individual
  {
// filenames: yalefaces/subject##.suffix
    String[] suffixes={
              "glasses",
              "happy",
              "leftlight",
              "noglasses",
              "normal",
              "rightlight",
              "sad",
              "sleepy",
              "surprised",
              "wink"
             };
    String filename="yalefaces/subject0"+subject+"."+suffixes[image];

    //maxScore=0;calculateMaxScore=true;
// load given file, step the system, check ID
    double thisScore=singleTest(subject,filename,e);
    return(thisScore);
  }

  double singleTest(int subject, String filename, EEXIST e) // subject=1 2 or 3
  {

    double score=0; // running score
    if (false) System.out.println("Loading subject " + subject + " filename=<" + filename + ">");

    e.clearAllTubes();
// okay here we go! For SL!

    BufferedImage img=null;
    try{
      img=ImageIO.read(new File(filename));
    } catch (Exception ex){
      System.out.println("Error reading " + filename + "(subject " + subject + "): " + ex);
      return(0);
    }

// load chemicals
// 0->40, dx=0.125, so 320x320
// load image in top 320x243; read outputs from lower left 20x20 regions 
    for (int y=0;y<243;y+=2){
      for (int x=0;x<320;x+=2){
        int c=img.getRGB(x,y); // full pixel value
        double red=(c&0xff);
        double green=((c>>8)&0xff);
        double blue=((c>>16)&0xff);
        red=red/7;green=green/7;blue=blue/7; // max ~37
        double alpha=(red+blue+green)/3.; // really this is the channel that matters
        //loadChems(x/8., y/8., red, green, blue, (red+green+blue)/3.); // rgb + alpha?
// make it visible...
        loadChems(x/16., y/16., alpha,alpha+5,alpha-7,alpha/3);
      }
    }

    // step 2500 ticks, then read value for 20 more
    for (int i=0;i<2500000;i++){
      e.step(1);  // pre-process/stabilize
    }

// sample outputs
    for (int i=0;i<20;i++){ 
      e.step(1);
      double s1=readChems(0,15,5,20);
      double s2=readChems(5,15,10,20);
      double s3=readChems(10,15,15,20);
      double s4=readChems(15,15,20,20);
      double s5=readChems(20,15,25,20); // output regions

// find max, avoid typos :)
      double max=0;int maxsub=0;
      if (s1 > max){max=s1;maxsub=1;}
      if (s2 > max){max=s2;maxsub=2;}
      if (s3 > max){max=s3;maxsub=3;}
      if (s4 > max){max=s4;maxsub=4;}
      if (s5 > max){max=s5;maxsub=5;}

      if (subject==maxsub) ++score; // :)

      //if (lastTestPassed) ++passedCount;
    } // end of tests
    return(score); // max=# of iterations
  }

/************************************
 * chemical etc read and write
 ************************************/

// read chems in a region and tally correct ones (y shows correct value)
  double readChems(double startX,double startY,double endX,double endY)
  {
    int tally=0;
    double sum=0; // just add all chemical levels within given region
    //int maxTally=0;
    for (double x=startX;x<endX;x+=e.getDx()){
      for (double y=startY;y<endY;y+=e.getDy()){
        sum+=e.getTube(x,y,EEXIST.SRCX) + e.getTube(x,y,EEXIST.SRCY) +
             e.getTube(x,y,EEXIST.DSTX) + e.getTube(x,y,EEXIST.DSTY);
        //if ((out && sum>15) || ((!out) && sum<15)) ++tally; // correct value
        //if (calculateMaxScore) ++maxScore;
        //++maxTally;
      }
    }
    return(sum); // use this for choosing survivors etc.
  }

// load a square region of chemicals (same value throughout!)
  void loadChems(double x, double y, double v1, double v2, double v3, double v4)
  {
    e.setTube(x,y,EEXIST.SRCX,v1);
    e.setTube(x,y,EEXIST.SRCY,v2);
    e.setTube(x,y,EEXIST.DSTX,v3);
    e.setTube(x,y,EEXIST.DSTY,v4);
  }

// set a square region of diameters
  void setDiams(double startX, double startY, double endX, double endY, double value)
  {
    for (double x=startX;x<endX;x+=e.getDx()){
      for (double y=startY;y<endY;y+=e.getDy()){
        e.setDiameter(x,y,value);
      }
    }
  }

/************************************
 * legacy code...
 ************************************/

  public void cpHandler(ControlArgs arg)
  {
    switch(arg.getUserID()){
    case 1: // karma
      e.setKarma(((double)arg.getSliderPos())/10);
      //writeStatus(pw,e); // write new EEXIST data
      System.out.println("\n\nKARMA SET TO " + e.getKarma() + "\n");
      break;
    }
  }

}
