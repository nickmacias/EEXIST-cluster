#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define SIZE 650

typedef struct FTube
{
	double SrcX = 0.0f;
	double SrcY = 0.0f;

	double DesX = 0.0f;
	double DesY = 0.0f;
};

typedef struct FBuffer
{
	FTube Buffer[SIZE][SIZE];
};

typedef struct FOffset
{
	int OffsetX = 0.0f;
	int OffsetY = 0.0f;
};

typedef struct FSystem
{
	//Chem Values At Each Point
	FTube Tubes[SIZE][SIZE];

	//Bias Offsets For Diffrent Chems
	FOffset BiasSrc[SIZE][SIZE];
	FOffset BiasDes[SIZE][SIZE];
};


int GetNumOfSystems();
double GetKarma();

int GenerateSystem(FSystem*, int);
int UpdateSystem(FSystem*, FBuffer*, double, double);
double GetChemToTake(double, double , double , double , double);
int GetCycles();

int main()
{
	printf("This Program Runs EEXIST And Writes Output To File EOUT.txt Visuals For This Can Then Be Studied Inside Sperate Unreal Engine Program.\n\n\n");

	//Get The Number Of EXIST System's To Create
	int NumOfSystems = GetNumOfSystems();
	//Get The Karma Value
	double Karma = GetKarma();

	//Create Array Of Systems Of Lenth Specified By User
	FSystem* Systems = (FSystem*)malloc(NumOfSystems * sizeof(FSystem));


	printf("\n\nGenerating Systems ...\n\n");

	//Loop Through Num Of Systems To Create And Call Function That Set DEfault Values
	for (int i = 0; i < NumOfSystems; i++)
	{
		//Pass Pointer To System And Genrate Default Values
		GenerateSystem(Systems, i);
	}

	printf("Generating Systems Complete\n\n");

	//Get How Many times the systems should be updated
	int Cycles = GetCycles();

	//This will hold calculated changes to the system update Decalre it now so it does not have to be re initalised on each system update
	FBuffer* Buffer = (FBuffer*)malloc(sizeof(FBuffer));

	//Iterate Through Number Of Cycles To Run
	for (int i = 0; i < Cycles; i++)
	{
		system("cls");

		printf("\n\nOn Cycle %d Out Of %d, Karma: %lf \n\n", i+1, Cycles, Karma);

		//Iterate Through The Number Of Systems
		for (int j = 0; j < NumOfSystems; j++)
		{
			//Update A System
			UpdateSystem(&Systems[j], Buffer, Karma, 10.0f);

			//TODO Output System Results To File
		}
	}
}

int GenerateSystem(FSystem* System, int SysID)
{
	//Iterate Through X Values Of Tubes
	for (int x = 0; x < sqrt(sizeof(System->Tubes) / 32); x++)
	{
		//Iterate Through Y Values Of Tubes
		for (int y = 0; y < sqrt(sizeof(System->Tubes) / 32); y++)
		{
			//Gen Random Source Chem Value
			System->Tubes[x][y].SrcX = (rand() % (int)sqrt(sizeof(System->Tubes) / 32));
			System->Tubes[x][y].SrcY = (rand() % (int)sqrt(sizeof(System->Tubes) / 32));


			//Gen Random Destination Chem Value
			System->Tubes[x][y].DesX = (rand() % (int)sqrt(sizeof(System->Tubes) / 32));
			System->Tubes[x][y].DesY = (rand() % (int)sqrt(sizeof(System->Tubes) / 32));


			//Genrate Random Bias (Genetic Code) NOTE: Subtract Coords to Keep Bias In Range
			System->BiasSrc[x][y].OffsetX = (rand() % (int)sqrt(sizeof(System->BiasSrc) / 8) - System->Tubes[x][y].SrcX);
			System->BiasSrc[x][y].OffsetY = (rand() % (int)sqrt(sizeof(System->BiasSrc) / 8) - System->Tubes[x][y].SrcY);

			System->BiasDes[x][y].OffsetX = (rand() % (int)sqrt(sizeof(System->BiasDes) / 8) - System->Tubes[x][y].DesX);
			System->BiasDes[x][y].OffsetY = (rand() % (int)sqrt(sizeof(System->BiasDes) / 8) - System->Tubes[x][y].DesY);
		}
	}

	return 1;
}

int UpdateSystem(FSystem* System, FBuffer* Buffer,  double Karma, double delta)
{
	//Clear Buffer
	for (int x = 0; x < SIZE; x++)
	{
		for (int y = 0; y < SIZE; y++)
		{
			Buffer->Buffer[x][y].DesX = 0.0f;
			Buffer->Buffer[x][y].DesY = 0.0f;

			Buffer->Buffer[x][y].SrcX = 0.0f;
			Buffer->Buffer[x][y].SrcY = 0.0f;
		}
	}

	//Iterate Through All Tubes in System (Calcuate Changes That Need To Be Made To The System)
	for (int x = 0; x < SIZE; x++)
	{
		for (int y = 0; y < SIZE; y++)
		{
			//Where to take chems from Sutract delta from this location
			int SrcX = (int)System->Tubes[x][y].SrcX;
			int SrcY = (int)System->Tubes[x][y].SrcY;

			//Where to put chemicals add delta to this location
			int DesX = (int)System->Tubes[x][y].DesX;
			int DesY = (int)System->Tubes[x][y].DesY;

			//printf("No Offset Take: %d, %d, Put: %d, %d\n\n", SrcX, SrcY, DesX, DesY);

			//Add Offset For Src (Bias)
			SrcX = SrcX + System->BiasSrc[x][y].OffsetX;
			SrcY = SrcY + System->BiasSrc[x][y].OffsetY;

			//Add Offset For Des (Bias)
			DesX = DesX + System->BiasDes[x][y].OffsetX;
			DesY = DesY + System->BiasDes[x][y].OffsetY;

			//Mod the Src And Dest To Keep Values In Range So Not taking chems or putting chems ouside of data array
			SrcX = (SrcX + SIZE) % SIZE;
			SrcY = (SrcY + SIZE) % SIZE;
			DesX = (DesX + SIZE) % SIZE;
			DesY = (DesY + SIZE) % SIZE;

			//Iterate Through All Points Within KArma Cordiante Points Away
			for (int Kx = -Karma; Kx < Karma; Kx++)
			{
				for (int Ky = -Karma; Ky < Karma; Ky++)
				{
					//Make Sure The Actaul Physical Disatnce From The Point To Source Is <= To Karma
					double KarmaDistance = sqrt(pow(SrcX + Kx, 2) + pow(SrcY + Ky, 2));
					if (KarmaDistance <= Karma)
					{
						//Fit Spots Around Karma Into Array Size
						int KSrcX = (SrcX + Kx + SIZE) % SIZE;
						int KSrcY = (SrcY + Ky + SIZE) % SIZE;
						int KDesX = (DesX + Kx + SIZE) % SIZE;
						int KDesY = (DesY + Ky + SIZE) % SIZE;

						//printf("\nOrgin KSrcX: %d KSrcY: %d KDesX: %d KDesY: %d", SrcX, SrcY, DesX, DesY);
						//printf("\nKarma Offset KSrcX: %d KSrcY: %d KDesX: %d KDesY: %d", KSrcX, KSrcY, KDesX, KDesY);

						//Calcaute How May Chemicals To Take From SOurce And Be TRansfered To Des
						double KSrcXTake = GetChemToTake(Karma, KarmaDistance, Buffer->Buffer[KSrcX][KSrcY].SrcX, Buffer->Buffer[KDesX][KDesY].SrcX, delta);
						double KSrcYTake = GetChemToTake(Karma, KarmaDistance, Buffer->Buffer[KSrcX][KSrcY].SrcY, Buffer->Buffer[KDesX][KDesY].SrcY, delta);

						double KDesXTake = GetChemToTake(Karma, KarmaDistance, Buffer->Buffer[KSrcX][KSrcY].DesX, Buffer->Buffer[KDesX][KDesY].DesX, delta);
						double KDesYTake = GetChemToTake(Karma, KarmaDistance, Buffer->Buffer[KSrcX][KSrcY].DesY, Buffer->Buffer[KDesX][KDesY].DesY, delta);

						//Take Chemicals From Src
						Buffer->Buffer[KSrcX][KSrcY].SrcX = Buffer->Buffer[KSrcX][KSrcY].SrcX - KSrcXTake;
						Buffer->Buffer[KSrcX][KSrcY].SrcY = Buffer->Buffer[KSrcX][KSrcY].SrcY - KSrcYTake;

						Buffer->Buffer[KSrcX][KSrcY].DesX = Buffer->Buffer[KSrcX][KSrcY].DesX - KDesXTake;
						Buffer->Buffer[KSrcX][KSrcY].DesY = Buffer->Buffer[KSrcX][KSrcY].DesY - KDesYTake;

						//Put Chemicals In Des
						Buffer->Buffer[KDesX][KDesY].SrcX = Buffer->Buffer[KDesX][KDesY].SrcX + KSrcXTake;
						Buffer->Buffer[KDesX][KDesY].SrcY = Buffer->Buffer[KDesX][KDesY].SrcY + KSrcYTake;

						Buffer->Buffer[KDesX][KDesY].DesX = Buffer->Buffer[KDesX][KDesY].DesX + KDesXTake;
						Buffer->Buffer[KDesX][KDesY].DesY = Buffer->Buffer[KDesX][KDesY].DesY + KDesYTake;
					}
				}
			}
		}
	}
	
	//Apply Calcuated Changes
	for (int x = 0; x < SIZE; x++)
	{
		for (int y = 0; y < SIZE; y++)
		{
			//Update Des Chemicals
			System->Tubes[x][y].DesX = System->Tubes[x][y].DesX + Buffer->Buffer[x][y].DesX;
			System->Tubes[x][y].DesY = System->Tubes[x][y].DesY + Buffer->Buffer[x][y].DesY;
			System->Tubes[x][y].SrcX = System->Tubes[x][y].SrcX + Buffer->Buffer[x][y].SrcX;
			System->Tubes[x][y].SrcY = System->Tubes[x][y].SrcY + Buffer->Buffer[x][y].SrcY;
		}
	}

	return 1;
}

double GetChemToTake(double Karma, double KarmaDistance, double CurrentChemLevel, double TargetChemLevel, double TargetAmountToTake)
{
	//Basically a precentage based on how far away this point is from Center
	double KarmaMultiplier = KarmaDistance / Karma;

	//Scale The Amount Of Chem Reuqesting By The Karma Diustance.
	TargetAmountToTake = TargetAmountToTake * KarmaMultiplier;

	//Make Sure There Is Some Chemical To Take
	if (CurrentChemLevel > 0)
	{
		//Check If The Is More Chemical Availble Than Requesting
		if (TargetChemLevel > TargetAmountToTake)
		{
			return TargetAmountToTake;
		}
		else
		{
			return TargetChemLevel;
		}
	}

	return 0.0f;
}

int GetNumOfSystems()
{
	char Input[120];
	int NumOfSystems = 0;

	//Get How Many Systems To Run
	printf("How Many Systems Do You Want To Run? (A System Is A Single Instance Of The EEXIST Program)\n");

	fgets(Input, 120, stdin);
	sscanf(Input, "%d", &NumOfSystems);

	return NumOfSystems;
}

double GetKarma()
{
	char Input[120];
	double Karma = 0.0f;

	//Get The Karma Value
	printf("What Value Do You Want For Karma? (Karma Is The Radius Of Which Checmical IS Tooken And Transfered To)\n");

	fgets(Input, 120, stdin);
	sscanf(Input, "%lf", &Karma);

	return Karma;
}

int GetCycles()
{
	char Input[120];
	int Cycles = 0;

	//Get The Karma Value
	printf("How Many Cycles Would You Like To Run? (This Is How Many Times The System Update Before Stoping NOTE: Can Be Lengthy Process)\n");

	fgets(Input, 120, stdin);
	sscanf(Input, "%d", &Cycles);

	return Cycles;
}

/*
	FILE* OUT = fopen("EOUT.txt", "w");

	if (OUT == NULL)
	{
		printf("Failed To Open TXT");
		return 0;
	}

	printf("\n\n\n\n\nWrote To File\n\n\n\n\n");

	fprintf(OUT, "I'm writting to this file");

	fclose(OUT);

	FILE* IN = fopen("EOUT.txt", "r");

	char Test[120];

	fscanf(IN, "%s", &Test);

	printf("\n\n\n\n Found: %s \n\n\n\n\n", Test);

	fclose(IN);
*/ 
