#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define SIZE 650

typedef struct FTube
{
	double SrcX = 0.0f;
	double SrcY = 0.0f;

	double DesX = 0.0f;
	double DesY = 0.0f;
};

typedef struct FBuffer
{
	FTube Buffer[SIZE][SIZE];
};

typedef struct FOffset
{
	int OffsetX = 0.0f;
	int OffsetY = 0.0f;
};

typedef struct FSystem
{
	//Chem Values At Each Point
	FTube Tubes[SIZE][SIZE];

	//Bias Offsets For Diffrent Chems
	FOffset BiasSrc[SIZE][SIZE];
	FOffset BiasDes[SIZE][SIZE];
};


int GetNumOfSystems();
double GetKarma();

int GenerateSystem(FSystem*, int);
int UpdateSystem(FSystem*, FBuffer*, double, double);
int GetCycles();

int main()
{
	printf("This Program Runs EEXIST And Writes Output To File EOUT.txt Visuals For This Can Then Be Studied Inside Sperate Unreal Engine Program.\n\n\n");

	//Get The Number Of EXIST System's To Create
	int NumOfSystems = GetNumOfSystems();
	//Get The Karma Value
	double Karma = GetKarma();

	//Create Array Of Systems Of Lenth Specified By User
	FSystem* Systems = (FSystem*)malloc(NumOfSystems * sizeof(FSystem));


	printf("\n\nGenerating Systems ...\n\n");

	//Loop Through Num Of Systems To Create And Call Function That Set DEfault Values
	for (int i = 0; i < NumOfSystems; i++)
	{
		//Pass Pointer To System And Genrate Default Values
		GenerateSystem(Systems, i);
	}

	printf("Generating Systems Complete\n\n");

	//Get How Many times the systems should be updated
	int Cycles = GetCycles();

	//This will hold calculated changes to the system update Decalre it now so it does not have to be re initalised on each system update
	FBuffer* Buffer = (FBuffer*)malloc(sizeof(FBuffer));

	//Iterate Through Number Of Cycles To Run
	for (int i = 0; i < Cycles; i++)
	{
		printf("\n\nOn Cycle %d\n\n", i + 1);

		//Iterate Through The Number Of Systems
		for (int j = 0; j < NumOfSystems; j++)
		{
			//Update A System
			UpdateSystem(&Systems[j], Buffer, Karma, 10.0f);

			//TODO Output System Results To File
		}

		printf("\n\nCycle Complete!! %d\n\n", i + 1);
	}
}

int GenerateSystem(FSystem* System, int SysID)
{
	//Iterate Through X Values Of Tubes
	for (int x = 0; x < sqrt(sizeof(System->Tubes) / 32); x++)
	{
		//Iterate Through Y Values Of Tubes
		for (int y = 0; y < sqrt(sizeof(System->Tubes) / 32); y++)
		{
			//Gen Random Source Chem Value
			System->Tubes[x][y].SrcX = (rand() % (int)sqrt(sizeof(System->Tubes) / 32));
			System->Tubes[x][y].SrcY = (rand() % (int)sqrt(sizeof(System->Tubes) / 32));


			//Gen Random Destination Chem Value
			System->Tubes[x][y].DesX = (rand() % (int)sqrt(sizeof(System->Tubes) / 32));
			System->Tubes[x][y].DesY = (rand() % (int)sqrt(sizeof(System->Tubes) / 32));


			//Genrate Random Bias (Genetic Code) NOTE: Subtract Coords to Keep Bias In Range
			System->BiasSrc[x][y].OffsetX = (rand() % (int)sqrt(sizeof(System->BiasSrc) / 8) - System->Tubes[x][y].SrcX);
			System->BiasSrc[x][y].OffsetY = (rand() % (int)sqrt(sizeof(System->BiasSrc) / 8) - System->Tubes[x][y].SrcY);

			System->BiasDes[x][y].OffsetX = (rand() % (int)sqrt(sizeof(System->BiasDes) / 8) - System->Tubes[x][y].DesX);
			System->BiasDes[x][y].OffsetY = (rand() % (int)sqrt(sizeof(System->BiasDes) / 8) - System->Tubes[x][y].DesY);
		}
	}

	return 1;
}

int UpdateSystem(FSystem* System, FBuffer* Buffer,  double Karma, double delta)
{
	//Clear Buffer
	for (int x = 0; x < SIZE; x++)
	{
		for (int y = 0; y < SIZE; y++)
		{
			Buffer->Buffer[x][y].DesX = 0.0f;
			Buffer->Buffer[x][y].DesY = 0.0f;

			Buffer->Buffer[x][y].SrcX = 0.0f;
			Buffer->Buffer[x][y].SrcY = 0.0f;
		}
	}

	//Iterate Through All Tubes in System (Calcuate Changes That Need To Be Made To The System)
	for (int x = 0; x < SIZE; x++)
	{
		for (int y = 0; y < SIZE; y++)
		{
			//Where to take chems from Sutract delta from this location
			int SrcX = (int)System->Tubes[x][y].SrcX;
			int SrcY = (int)System->Tubes[x][y].SrcY;

			//Where to put chemicals add delta to this location
			int DesX = (int)System->Tubes[x][y].DesX;
			int DesY = (int)System->Tubes[x][y].DesY;

			//printf("No Offset Take: %d, %d, Put: %d, %d\n\n", SrcX, SrcY, DesX, DesY);

			//Add Offset For Src (Bias)
			SrcX = SrcX + System->BiasSrc[x][y].OffsetX;
			SrcY = SrcY + System->BiasSrc[x][y].OffsetY;

			//Add Offset For Des (Bias)
			DesX = DesX + System->BiasDes[x][y].OffsetX;
			DesY = DesY + System->BiasDes[x][y].OffsetY;

			//printf(" Offset Value: Take: %d, %d, Put: %d, %d\n\n", System->BiasSrc[x][y].OffsetX, System->BiasSrc[x][y].OffsetY, System->BiasDes[x][y].OffsetX, System->BiasDes[x][y].OffsetY);
			//printf("With Offset :Take: %d, %d, Put: %d, %d\n\n", SrcX, SrcY, DesX, DesY);

			///TODO Add in KARMA
			//Take Chems From Sorce
			Buffer->Buffer[SrcX][SrcY].SrcX = Buffer->Buffer[SrcX][SrcY].SrcX - delta;
			Buffer->Buffer[SrcX][SrcY].SrcY = Buffer->Buffer[SrcX][SrcY].SrcY - delta;
			Buffer->Buffer[SrcX][SrcY].DesX = Buffer->Buffer[SrcX][SrcY].DesX - delta;
			Buffer->Buffer[SrcX][SrcY].DesY = Buffer->Buffer[SrcX][SrcY].DesY - delta;

			

			//Add Chems To Destination
			Buffer->Buffer[DesX][DesY].SrcX = Buffer->Buffer[DesX][DesY].SrcX + delta;
			Buffer->Buffer[DesX][DesY].SrcY = Buffer->Buffer[DesX][DesY].SrcY + delta;
			Buffer->Buffer[DesX][DesY].DesX = Buffer->Buffer[DesX][DesY].DesX + delta;
			Buffer->Buffer[DesX][DesY].DesY = Buffer->Buffer[DesX][DesY].DesY + delta;
		}
	}
	
	//Apply Calcuated Changes
	for (int x = 0; x < SIZE; x++)
	{
		for (int y = 0; y < SIZE; y++)
		{
			//Update Des and Src Chemicals
			System->Tubes[x][y].DesX = System->Tubes[x][y].DesX + Buffer->Buffer[x][y].DesX;
			System->Tubes[x][y].DesY = System->Tubes[x][y].DesY + Buffer->Buffer[x][y].DesY;
			System->Tubes[x][y].SrcX = System->Tubes[x][y].SrcX + Buffer->Buffer[x][y].SrcX;
			System->Tubes[x][y].SrcY = System->Tubes[x][y].SrcY + Buffer->Buffer[x][y].SrcY;

			//printf("X: %d, Y: %d\n", x, y);
		}
	}

	return 1;
}

int GetNumOfSystems()
{
	char Input[120];
	int NumOfSystems = 0;

	//Get How Many Systems To Run
	printf("How Many Systems Do You Want To Run? (A System Is A Single Instance Of The EEXIST Program)\n");

	fgets(Input, 120, stdin);
	sscanf(Input, "%d", &NumOfSystems);

	return NumOfSystems;
}

double GetKarma()
{
	char Input[120];
	double Karma = 0.0f;

	//Get The Karma Value
	printf("What Value Do You Want For Karma? (Karma Is The Radius Of Which Checmical IS Taken And Transfered To)\n\n");

	fgets(Input, 120, stdin);
	sscanf(Input, "%f", &Karma);

	return Karma;
}

int GetCycles()
{
	char Input[120];
	int Cycles = 0;

	//Get The Karma Value
	printf("How Many Cycles Would You Like To Run? (This Is How Many Times The System Update Before Stoping NOTE: Can Be Lengthy Process)\n\n");

	fgets(Input, 120, stdin);
	sscanf(Input, "%d", &Cycles);

	return Cycles;
}
